package com.vinbytes.binary.controller;

import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.*;
import com.vinbytes.binary.repository.PlayBidRepository;
import com.vinbytes.binary.repository.UserRepository;
import com.vinbytes.binary.services.NotificationService;
import com.vinbytes.binary.services.PlayBidServices;
import io.micrometer.core.instrument.Counter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Api(tags = "Play Bids Controller")
@CrossOrigin("*")
@RestController
@RequestMapping(value="/playbid")
public class PlayBidController extends PlayBidServices {
    @Autowired
    private PlayBidRepository playBidRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MongoOperations mongoOperations;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private NotificationService notificationService;

    private static final Logger log = LoggerFactory.getLogger(PlayBidController.class);


    @ApiOperation(value = "Create Play Bids", notes = "POST API to create play bids manually.")
    @PostMapping(value = "/create/{minutes}")
    public ResponseEntity<?> createPlayBid(@ApiParam(value = "Minutes Of Bids Creation", required = true) @PathVariable int minutes){
        try {
            log.info("inside PlayBid createPlayBid !");
            if (minutes == 3 || minutes == 5 || minutes == 15 || minutes == 30){
                Date currentServerDate = new Date();
                Date closeDate = new Date();
                Date drawDate = new Date();
                closeDate.setMinutes(closeDate.getMinutes() + (minutes-1));
                drawDate.setMinutes(drawDate.getMinutes() + minutes);

                /*Bitcoin Stock*/
                PlayBidModel bitcoinModel = new PlayBidModel("Bitcoin", currentServerDate, currentServerDate, closeDate, drawDate, String.valueOf(minutes), "5","Open Now", "BTC", "assets/images/home/bitcoin.png");
                boolean bitcoin = create(bitcoinModel);
/*

                *//*Tether Stock*//*
                PlayBidModel tetherModel = new PlayBidModel("Tether", currentServerDate, currentServerDate, closeDate, drawDate, String.valueOf(minutes), "5", "USDT", "assets/images/home/tether.png");
                boolean tether = create(tetherModel);


                *//*Ethereum Stock*//*
                PlayBidModel ethereumModel = new PlayBidModel("Ethereum", currentServerDate, currentServerDate, closeDate, drawDate, String.valueOf(minutes), "5", "ETH", "assets/images/home/ethereum.png");
                boolean ethereum = create(ethereumModel);


                *//*Dogecoin Stock*//*
                PlayBidModel dogeCoinModel = new PlayBidModel("Dogecoin", currentServerDate, currentServerDate, closeDate, drawDate, String.valueOf(minutes), "5", "DOGE", "assets/images/home/dogeCoin.png");
                boolean dogeCoin = create(dogeCoinModel);


                *//*Binance Coin Stock*//*
                PlayBidModel BinanceCoinModel = new PlayBidModel("Binance Coin", currentServerDate, currentServerDate, closeDate, drawDate, String.valueOf(minutes), "5", "BNB", "assets/images/home/BinanceCoin.png");
                boolean binanceCoin = create(BinanceCoinModel);*/

                if (bitcoin ){
                    /*List<UserModel> frmDb = userRepository.findAll();
                    for (UserModel frmDb1  : frmDb) {

                        notificationService.MakeNotifications(frmDb1.getId(), "New "+minutes+" mins opened now.", "GET!SET!GO!!!", frmDb1.getProfileUrl(), "/notification");
                    }*/

                    return ResponseEntity.ok(new MessageResponse("Play Bid - Bidding Details Created successfully!"));
                }else {
                    return ResponseEntity.badRequest().body(new MessageResponse("PlayBid Creation Unsuccessful !"));
                }
            }else {
                return ResponseEntity.badRequest().body(new MessageResponse("PlayBid Creation Unsuccessful - Time Exception!"));
            }
        } catch (Exception e){
            return ResponseEntity.badRequest().body(new MessageResponse("Error info "+e));
        }
    }

    @ApiOperation(value = "Get Play Bid By ID", notes = "GET API to get play bid by given ID.")
    @GetMapping(value = "/get/BidId/{playBidID}")
    public ResponseEntity<PlayBidModel> getAllPlayBidDetailsByBidID(@ApiParam(value = "GetPlayBidById Object", required = true) @PathVariable String playBidID) {
        log.info("inside PlayBidController.getAllPlayBidDetailsByBidID() method");

        PlayBidModel model = playBidRepository.findByBidId(playBidID);
        if (model == null) {
            return ResponseEntity.badRequest().header("Custom_Header_of_PlayBid", "PlayBidID Not Found").body(model);
        } else {
            List<LiveBidders> LiveBidders1=new ArrayList<>();
                if (model != null && model.getUserId() != null && model.getUserId().size() > 0) {
                    for (String id : model.getUserId()) {
                        if (id != null) {
                            UserModel userFrmDb = userRepository.findById(id).orElse(null);

                            LiveBidders liveBidders = new LiveBidders(userFrmDb.getUsername(), userFrmDb.getProfileUrl());
                            LiveBidders1.add(liveBidders);
                        }
                    }
                    model.setLiveBidders(LiveBidders1);
                }

            return ResponseEntity.ok().header("Custom_Header_of_PlayBid", "Get All Details of PlayBidID")
                    .body(model);
        }
    }

    @ApiOperation(value = "Get All Play Bids", notes = "GET API to get all play bids details.")
    @GetMapping(value = "/getAll")
    @ApiParam(value = "PlayBidDetails Object", required = true)
    public ResponseEntity<List<PlayBidModel>> getAllPlayBidDetails() {

        log.info("inside PlayBidController.getAllPlayBidDetails() method");

        List<PlayBidModel> frmDb = playBidRepository.findAllByBiddingStatus("Open Now");
        return ResponseEntity.ok().body(frmDb);


    }

    @ApiOperation(value = "Get All Play Bids", notes = "GET API to get all play bids.")
    @GetMapping(value = "/All")
    @ApiParam(value = "GetAllPlayBid Object", required = true)
    public ResponseEntity<Response> GetAllPlayBid(
            @PageableDefault(page = 0, size = 10,sort = "bidId", direction = Sort.Direction.DESC) Pageable pageable) {

        Response res=new Response();
        try {
            log.info("inside PlayBidController.getAllPlayBidDetails() method");
            Page<PlayBidModel> model = playBidRepository.findAll(pageable);
            res.setStatus("success");
            res.setPageData(model);
            return new ResponseEntity<Response>(res, HttpStatus.OK);
        }
        catch (Exception e){
            res.setStatus("success");
            res.setResponseMessage("error "+e);
            return new ResponseEntity<Response>(res, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ApiOperation(value = "Get All Play Bids", notes = "GET API to get all play bids Status with count.")
    @GetMapping(value = "/All/count")
    @ApiParam(value = "GetAllPlayBidCount Object", required = true)
    public ResponseEntity<BidsStatusCountModel> GetAllPlayBidCount() {
        log.info("inside PlayBidController.GetAllPlayBidCount() method");
        BidsStatusCountModel model = new BidsStatusCountModel();
        model.setOpeningSoon(playBidRepository.countByBiddingStatus("Opening Soon"));
        model.setOpenNow(playBidRepository.countByBiddingStatus("Open Now"));
        model.setClosed(playBidRepository.countByBiddingStatus("Closed"));
        model.setCompleted(playBidRepository.countByBiddingStatus("Completed"));
        model.setCancelled(playBidRepository.countByBiddingStatus("Cancelled"));
        return ResponseEntity.ok().header("Custom_Header_of_PlayBid", "Get All Details of PlayBidID").body(model);
    }

    @ApiOperation(value = "Get All Play Bids", notes = "GET API to get all play bids.")
    @GetMapping(value = "/search/byStatus/{status}")
    @ApiParam(value = "GetAllPlayBid Object", required = true)
    public ResponseEntity<Response> getplaybidByStatus(
            @PathVariable("status") String status,
            @PageableDefault(page = 0, size = 10,sort = "bidId", direction = Sort.Direction.DESC) Pageable pageable) {

        Response res=new Response();
        try {
            log.info("inside PlayBidController.getAllPlayBidDetails() method");
            Page<PlayBidModel> model;
            if(status.equals("All"))
            {
             model = playBidRepository.findAll(pageable);
            }
            else
            {
              model = playBidRepository.findAllByBiddingStatus(status,pageable);
            }
            res.setStatus("success");
            res.setPageData(model);
            return new ResponseEntity<Response>(res, HttpStatus.OK);
        }
        catch (Exception e){
            res.setStatus("success");
            res.setResponseMessage("error "+e);
            return new ResponseEntity<Response>(res, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



    @ApiOperation(value = "Search ByStockName Records", notes = "GET API to Fetch All Popular Products", response = List.class)
    @GetMapping(value = "/Search/ByStockName/{Status}/{search}")
    public ResponseEntity<?> searchAllProductsByProductName
            (@PathVariable String Status,@PathVariable String search,
             @PageableDefault(page = 0, size = 10,sort = "bidId", direction = Sort.Direction.DESC) Pageable pageable)
    {
        log.info("inside PlayBidController.ByStockName() method");

        Query query = new Query();
        List<Criteria> criteria = new ArrayList<Criteria>();
        criteria.add(Criteria.where("stockName").regex(search, "i"));
              criteria.add(Criteria.where("symbol").regex(search, "i"));
              if(search.matches("[0-9]+")&& search.length()==4)
              {
                  query.addCriteria(Criteria.where("bidId").is(Long.parseLong(search)).
                          andOperator(Criteria.where("biddingStatus").is(Status)));

              }
              else
              {
                  query.addCriteria(Criteria.where("biddingStatus").is(Status)
                .andOperator(new Criteria().orOperator(criteria.toArray(new Criteria[criteria.size()]))));
              }
//

       List<PlayBidModel> model1 = mongoOperations.find(query,PlayBidModel.class);

        if (model1.size()==0) {

            return ResponseEntity.badRequest().header("Custom_Header_of_PlayBid", "search Not Found").body(model1);
        } else {
            Page<PlayBidModel> model = PageableExecutionUtils.getPage(
                    model1,
                    pageable,
                    () ->  mongoTemplate.count(query, PlayBidModel.class));
            return ResponseEntity.ok().header("Custom_Header_of_Product", "Get All Popular Products Successfully")
                    .body(model);
        }

    }

}
