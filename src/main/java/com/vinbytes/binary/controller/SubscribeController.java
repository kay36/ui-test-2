package com.vinbytes.binary.controller;

import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.NotifyMeModel;
import com.vinbytes.binary.repository.NotifyMeRepository;
import com.vinbytes.binary.services.EmailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Api(tags = "Subscribe Controller")

@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)

@RestController
@RequestMapping("API/v1/Public/Notify")
public class SubscribeController {

    private static final Logger log = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private NotifyMeRepository notifyMeRepository;

    @Autowired
    private EmailService emailService;

    @ApiOperation(value = "Notify Me", notes = "POST API for sending notification to USER.")
    @PostMapping(value = "/notifyMe")
    public ResponseEntity<Object> notifyMe(@ApiParam(value = "NotifyMe Object", required = true) @RequestBody NotifyMeModel notifyMeModel) throws MessagingException {
        try {
            log.info("inside AuthController.notifyMe() method");

            if(notifyMeModel.getEmail()== null || notifyMeModel.getEmail().equals("")){
                return ResponseEntity.badRequest().body(new MessageResponse("Email Id should not be empty."));
            }else if(notifyMeRepository.existsByEmail(notifyMeModel.getEmail())){
                return ResponseEntity.badRequest().body(new MessageResponse("Email Id '" + notifyMeModel.getEmail() + "' is already Subscribed!"));
            }

            NotifyMeModel notify = new NotifyMeModel();
            notify.setEmail(notifyMeModel.getEmail());
            notify.setEmailEnteredTime(LocalDateTime.now(ZoneId.of("Asia/Kolkata")));
            notify.setDevice(notifyMeModel.getDevice());
            notifyMeRepository.save(notify);

            emailService.sendMail("User",notifyMeModel.getEmail(), "Notification Request",
                    "https://play-crypto.com/");
            return ResponseEntity.ok(new MessageResponse("Welcome mail Sent to your mail Id Successfully"));
        } catch (Exception e){
            return ResponseEntity.badRequest().body(new MessageResponse("Error info "+e));
        }
    }
}
