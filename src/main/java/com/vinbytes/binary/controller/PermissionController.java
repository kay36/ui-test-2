package com.vinbytes.binary.controller;

import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.*;
import com.vinbytes.binary.repository.PermissionsRepository;
import com.vinbytes.binary.repository.UserRepository;
import com.vinbytes.binary.services.KeyCloakService;
import com.vinbytes.binary.services.UserDetailsImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Api(tags = "Permission Controller")
@CrossOrigin("*")
@RestController
/*JUN 08*/
@RequestMapping(value="/permissions")
public class PermissionController {

    private static final Logger log = LoggerFactory.getLogger(PermissionController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PermissionsRepository permissionsRepository;

    @Autowired
    private KeyCloakService keyCloakService;

    @RequestMapping(value = "/**", method = RequestMethod.OPTIONS)
    public void corsHeaders(HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        response.addHeader("Access-Control-Allow-Headers", "origin, content-type, accept, x-requested-with");
        response.addHeader("Access-Control-Max-Age", "3600");
    }

    @ApiOperation(value = "Permission", notes = "post api to update Permissions")
    @PostMapping("/update")
    public ResponseEntity<Object> SetPermissions(@ApiParam(value = "Permissions", required = true) @Valid @RequestBody PermissionModel permissionModel){
        log.info("inside PermissionController.SetPermissions() method");
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String userName = principal.toString();
            String userEmail = keyCloakService.getUserWithName(userName);
            if (!userEmail.equals("User Not Found !")) {
                UserModel user = userRepository.findByEmail(userEmail);
                if (user == null) {
                    return new ResponseEntity<>("User" + userName + " Not Found !", HttpStatus.NOT_FOUND);
                }else {
                    PermissionModel permissionModelFromDB = permissionsRepository.findByUserId(user.getId());
                    if (permissionModelFromDB != null){
                        permissionModel.setId(permissionModelFromDB.getId());
                    }
                    permissionModel.setUserId(user.getId());
                    permissionsRepository.save(permissionModel);
                    return ResponseEntity.ok(new MessageResponse("User Permissions successfully updated"));
                }
            }else {
                return new ResponseEntity<>("User Not Found, Invalid Access Token !", HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Permission", notes = "Get api to get Permissions of user")
    @GetMapping("/get")
    public ResponseEntity<Object> GetPermissions(){
        log.info("inside PermissionController.GetPermissions() method");
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String userName = principal.toString();
            String userEmail = keyCloakService.getUserWithName(userName);

            if (!userEmail.equals("User Not Found !")) {
                UserModel user = userRepository.findByEmail(userEmail);
                if (user == null) {
                    return new ResponseEntity<>("User" + userName + " Not Found !", HttpStatus.NOT_FOUND);
                }else {
                    PermissionModel permissionModelFromDB = permissionsRepository.findByUserId(user.getId());
                    if (permissionModelFromDB==null){
                        return new ResponseEntity<>("Permissions Not Found", HttpStatus.NOT_FOUND);
                    }else {
                        return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "Get Permissions Successful")
                                .body(permissionModelFromDB);
                    }
                }
            }else {
                return new ResponseEntity<>("User Not Found, Invalid Access Token !", HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Permission", notes = "set api to set Permissions of user at initial stage")
    @GetMapping("/set/init/{userId}")
    public String setInitPermissions(@PathVariable("userId") String userId){
        log.info("inside PermissionController.setInitPermissions() method");
        try {
            PermissionModel permissionModel = new PermissionModel();
            permissionModel.setUserId(userId);
            PermitNotificationModel permitNotificationModel = new PermitNotificationModel(false, false,false);
            PermitNotificationModel permitNotificationModelEmailTrue = new PermitNotificationModel(true, false,false);
            List<String> allSubCategoryOfGeneral = new ArrayList<>();
            List<String> allSubCategoryOfBids = new ArrayList<>();
            allSubCategoryOfGeneral.add("Password Reset successful");
            allSubCategoryOfGeneral.add("Forgot Password Request initiated");
            allSubCategoryOfGeneral.add("Latest Updates, Newsletters");
            allSubCategoryOfBids.add("Bid Purchase successful");
            allSubCategoryOfBids.add("Bid draw results");
            allSubCategoryOfBids.add("New Bid open notification");

            List<PermissionCategoryModel> permissionCategoryModelList = new ArrayList<>();
            for(int i=0; i<2;i++){
                PermissionCategoryModel permissionCategoryModel = new PermissionCategoryModel();
                if (i==0){
                    permissionCategoryModel.setCategory("General");
                    List<PermissionSubCategoryModel> permissionSubCategoryModelList = new ArrayList<>();
                    for(String SubCategory : allSubCategoryOfGeneral){
                        PermissionSubCategoryModel permissionSubCategoryModel = new PermissionSubCategoryModel();
                        permissionSubCategoryModel.setName(SubCategory);
                        if(SubCategory.equals("Password Reset successful")
                                || SubCategory.equals("Forgot Password Request initiated")){
                            permissionSubCategoryModel.setAttr(permitNotificationModelEmailTrue);
                        }else {
                            permissionSubCategoryModel.setAttr(permitNotificationModel);
                        }
                        permissionSubCategoryModelList.add(permissionSubCategoryModel);
                    }
                    permissionCategoryModel.setSubCategory(permissionSubCategoryModelList);
                }else{
                    permissionCategoryModel.setCategory("Bids");
                    List<PermissionSubCategoryModel> permissionSubCategoryModelList = new ArrayList<>();
                    for(String SubCategory : allSubCategoryOfBids){
                        PermissionSubCategoryModel permissionSubCategoryModel = new PermissionSubCategoryModel();
                        permissionSubCategoryModel.setName(SubCategory);
                        if(SubCategory.equals("Bid Purchase successful")){
                            permissionSubCategoryModel.setAttr(permitNotificationModelEmailTrue);
                        }else {
                            permissionSubCategoryModel.setAttr(permitNotificationModel);
                        }
                        permissionSubCategoryModelList.add(permissionSubCategoryModel);
                    }
                    permissionCategoryModel.setSubCategory(permissionSubCategoryModelList);
                }
                permissionCategoryModelList.add(permissionCategoryModel);
            }
            permissionModel.setPermissions(permissionCategoryModelList);
            permissionsRepository.save(permissionModel);
            return "set Permissions of user at initial stage was successful";
        }catch (Exception e){
            return "set Permissions of user at initial stage was unsuccessful";
        }
    }
}
