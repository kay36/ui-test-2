package com.vinbytes.binary.controller;

import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.*;
import com.vinbytes.binary.repository.DynamicSlabRepository;
import com.vinbytes.binary.repository.TransactionRepository;
import com.vinbytes.binary.services.PurchaseBidsServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "Purchase Bids Controller")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)
@RestController
@RequestMapping(value="/purchase")
public class PurchaseBidsController extends PurchaseBidsServices {


    @Autowired
    private MongoOperations mongoOperations;
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private TransactionRepository transactionRepository;


    @Autowired
    private DynamicSlabRepository dynamicSlabRepository;

    private static final Logger log = LoggerFactory.getLogger(PlayBidController.class);

    @ApiOperation(value = "Purchase Bid", notes = "POST API to purchase bid.")
    @PostMapping(value = "/create")
    public ResponseEntity<?> createPurchaseBids(@ApiParam(value = "CreatePurchaseBid Object", required = true) @RequestBody PurchaseBidsModel purchaseBidsModel) {
        try {
            return create(purchaseBidsModel);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info " + e.getMessage()));
        }
    }

    @ApiOperation(value = "Get Purchase Bid Details By StockName", notes = "GET API to retrieve all purchase bids by stock name.")
    @GetMapping(value = "/get/StockName/{purchaseStockName}")
    public ResponseEntity<?> getPurchaseDetailsByStockName(@ApiParam(value = "GetPurchaseDetailsByStockName Object", required = true) @PathVariable String purchaseStockName) {
        log.info("inside PlayBidController.getAllPlayBidDetailsByBidID() method");
        Query query = new Query();
        query.addCriteria(Criteria.where("stockName").is(purchaseStockName));
        List<PurchaseBidsModel> model = mongoOperations.find(query, PurchaseBidsModel.class);
        if (model.size() == 0) {
            return ResponseEntity.badRequest().body(new MessageResponse("Stock name Not Found"));
        } else {
            return ResponseEntity.ok().header("Custom_Header_of_PurchaseBids", "Get All Details of given stock name")
                    .body(model);
        }
    }

    @ApiOperation(value = "Get All Purchased Bids", notes = "GET API to retrieve all purchase bids.")
    @GetMapping(value = "/getAll")
    @ApiParam(value = "GetAllPurchasedBids Object", required = true)
    public ResponseEntity<?> getAllPurchaseBidsDetails() {
        log.info("inside PurchaseBidsController.getAllPurchaseBidsDetails() method");
        List<PurchaseBidsModel> model = purchaseBidsRepository.findAll();
        if (model.size() == 0) {
            return ResponseEntity.badRequest().body(new MessageResponse("Purchase bids Not Found"));
        } else {
            return ResponseEntity.ok().header("Custom_Header_of_PurchaseBids", "Get All purchased bids Successfully")
                    .body(model);
        }
    }

    @ApiOperation(value = "Get Bid by BidId", notes = "GET API to retrieve Bid by bid ID.")
    @GetMapping(value = "/get/user/{id}")
    public ResponseEntity<Object> getById(@ApiParam(value = "GetPurchasedBidsByBidID Object", required = true) @PathVariable("id") String id) {
        try {
            PlayBidModel model = playBidRepository.findByBidId(id);
            if (model == null) {
                return new ResponseEntity<>("Bid Id " + id + " does not exists", HttpStatus.NOT_FOUND);
            } else {
                return ResponseEntity.ok().header("Custom_Header_of_PurchaseBids", "Get All purchased bids Successfully")
                        .body(model);
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info " + e));
        }
    }

    @ApiOperation(value = "Get MyBids By USER Id", notes = "GET API to retrieve all bids by USER ID.")
    @GetMapping(value = "/get/user/mybids/{id}")
    public ResponseEntity<Object> getMyBids(@ApiParam(value = "GetAllBidsByUserID Object", required = true) @PathVariable("id") String id) {
        try {
            List<PurchaseBidsModel> model = purchaseBidsRepository.findAllByUserId(id);
            if (model.size() == 0) {
                return ResponseEntity.badRequest().body(new MessageResponse("Purchase bids Not Found"));
            } else {


                return ResponseEntity.ok().header("Custom_Header_of_PurchaseBids", "Get All purchased bids Successfully")
                        .body(model);
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info " + e));
        }
    }


    @ApiOperation(value = "Get MyBids and USER Id", notes = "GET API to retrieve  bids and USER ID.")
    @GetMapping(value = "/get/user/bids/{userId}/{bidId}")
    public ResponseEntity<Object> getPurchasedBids(
            @ApiParam(value = "GetBidsAndUserID Object", required = true) @PathVariable("userId") String userId, @PathVariable("bidId") String bidId) {
        try {
            List<PurchaseBidsModel> model = purchaseBidsRepository.findAllByStockIdAndUserId(bidId, userId);
            if (model.size() >= 3) {
                return ResponseEntity.badRequest().body(new MessageResponse("Reached Maximum limit for this bid"));
            } else {


                return ResponseEntity.ok().header("Custom_Header_of_PurchaseBids", "Get All purchased bids Successfully")
                        .body(new MessageResponse("success"));
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info " + e));
        }
    }



    @ApiOperation(value = "Get MyBids and USER Id", notes = "GET API to retrieve  bids and USER ID.")
    @GetMapping(value = "/get/user/purchased/{userId}/{bidId}")
    public ResponseEntity<Object> getPurchasedBidsInfo(
            @ApiParam(value = "GetBidsAndUserID Object", required = true) @PathVariable("userId") String userId, @PathVariable("bidId") String bidId) {
        try {
            List<PurchaseBidsModel> model = purchaseBidsRepository.findAllByStockIdAndUserId(bidId, userId);
            System.out.println(model.size());
            if (model.size() <1) {
                return ResponseEntity.badRequest().body(new MessageResponse("not Found"));
            } else {
                return ResponseEntity.ok().header("Custom_Header_of_PurchaseBids", "Get  purchased bids Successfully")
                        .body(model);
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info " + e.getMessage()));
        }
    }

    /*JUL 02*/
    @ApiOperation(value = "Get MyBids and USER Id", notes = "GET API to retrieve  bids and USER ID.")
    @GetMapping(value = "/get/all/bids/{userId}/{bidId}")
    public ResponseEntity<Object> getAllPurchasedBids(
            @ApiParam(value = "GetBidsAndUserID Object", required = true) @PathVariable("userId") String userId, @PathVariable("bidId") String bidId) {
        try {
            UserModel userModel = userRepository.findById(userId).orElse(null);
            if (userModel == null){
                return ResponseEntity.badRequest().body(new MessageResponse("No User Found !"));
            }else {
                List<PurchaseBidsModel> model = purchaseBidsRepository.findAllByStockIdAndUserId(bidId, userId);
                if (model.size() == 0) {
                    return ResponseEntity.badRequest().body(new MessageResponse("No Bids Found !"));
                } else {
                    List<PurchaseWithUserInfoModalForAdmin> purchaseWithUserInfoModalForAdmins = new ArrayList<>();
                    for (PurchaseBidsModel onePurchase : model){
                        PurchaseWithUserInfoModalForAdmin onePurchaseWithUser = new PurchaseWithUserInfoModalForAdmin();
                        TransactionModal transactionModal = transactionRepository.findByTransactionId(onePurchase.getTransactionId());
                        if (transactionModal!=null){
                            onePurchaseWithUser.setTransactionModal(transactionModal);
                        }
                        onePurchaseWithUser.setPurchase(onePurchase);
                        onePurchaseWithUser.setUserModel(userModel);
                        purchaseWithUserInfoModalForAdmins.add(onePurchaseWithUser);
                    }
                    return ResponseEntity.ok().header("Custom_Header_of_PurchaseBids", "Get All purchased bids Successfully")
                            .body(purchaseWithUserInfoModalForAdmins);
                }
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info " + e));
        }
    }


    /*JUN 04*/
    @ApiOperation(value = "Get Count with Range of BID", notes = "Get Count with Range of BID")
    @GetMapping(value = "/get/mybids/count/{bidId}")
    public ResponseEntity<Object> getBidsCountWithRange(@ApiParam(value = "GetAllBidsCountByBidId Object", required = true) @PathVariable("bidId") String bidId) {
        PlayBidModel playBidModel = playBidRepository.findByBidId(bidId);
        if (playBidModel == null) {
            return ResponseEntity.badRequest().body(new MessageResponse("Not a valid Bid Id"));
        } else {
            try {
                List<RangeCountModel> rangeCountModels = new ArrayList<>();
                DynamicSlabModel dynamicSlabModel = dynamicSlabRepository.findByType(playBidModel.getBidType());
                if (dynamicSlabModel == null){
                    return ResponseEntity.badRequest().body(new MessageResponse("Dynamic Slab Not Found !"));
                }else {
                    List<DynamicSlabValues> dynamicSlabValuesList = dynamicSlabModel.getDynamicSlabValues();
                    List<String> ranges = new ArrayList<>();
                    for (DynamicSlabValues dynamicSlabValues : dynamicSlabValuesList){
                        long fromValue = Math.round(dynamicSlabValues.getFromValue());
                        long toValue = Math.round(dynamicSlabValues.getToValue());
                        if (fromValue == toValue){
                            ranges.add("over $"+fromValue);
                        }else {
                            ranges.add("$"+fromValue+" - $"+toValue);
                        }
                    }

                    for (String rangeString : ranges) {
                        RangeCountModel rangeCount = new RangeCountModel();
                        rangeCount.setRange(rangeString);
                        List<PurchaseBidsModel> purchaseBidsModelsUp = purchaseBidsRepository.findAllByStockIdAndLevelAndChangesBtwAndPurchaseStatus(bidId, "Up", rangeString,"Success");
                        List<PurchaseBidsModel> purchaseBidsModelsDown = purchaseBidsRepository.findAllByStockIdAndLevelAndChangesBtwAndPurchaseStatus(bidId, "Down", rangeString,"Success");
                        rangeCount.setUp(String.valueOf(purchaseBidsModelsUp.size()));
                        rangeCount.setDown(String.valueOf(purchaseBidsModelsDown.size()));
                        rangeCountModels.add(rangeCount);
                    }
                    return ResponseEntity.ok().header("Custom_Header_of_PurchaseBids", "Get Count with Range of BID Successful")
                            .body(rangeCountModels);
                }
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(new MessageResponse("Error info " + e));
            }
        }
    }


    @ApiOperation(value = "Get All Play Bids", notes = "GET API to get all play bids.")
    @GetMapping(value = "/search/byStatus/{userId}/{status}")
    @ApiParam(value = "GetAllPlayBid Object", required = true)
    public ResponseEntity<Response> getplaybidByStatus(
            @PathVariable("userId") String userId, @PathVariable("status") String status,
            @PageableDefault(page = 0, size = 10, sort = "biddingDate", direction = Sort.Direction.DESC) Pageable pageable) {

        Response res = new Response();
        try {
            log.info("inside PurchaseBidsModel.getAllPurchaseBidDetails() method");
            Page<PurchaseBidsModel> model;
            if (status.equals("All")) {
                model = purchaseBidsRepository.findAllByUserId(userId, pageable);
            } else {
                model = purchaseBidsRepository.findAllByUserIdAndStatus(userId, status, pageable);
            }
            for (PurchaseBidsModel purchaseBidsModel : model) {
                purchaseBidsModel.setStockDetails(playBidRepository.findByBidId(purchaseBidsModel.getStockId()));
            }
            res.setStatus("success");
            res.setPageData(model);
            return new ResponseEntity<Response>(res, HttpStatus.OK);
        } catch (Exception e) {
            res.setStatus("success");
            res.setResponseMessage("error " + e);
            return new ResponseEntity<Response>(res, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @ApiOperation(value = "Get Play Bids By id", notes = "GET API to get all play bids.")
    @GetMapping(value = "/get/bid/{transactionId}")
    public ResponseEntity<?> getplaybidByStatus(@PathVariable("transactionId") String transactionId) {

        try {
            log.info("inside PurchaseBidsModel.getplaybidByStatus() method");
            PurchaseBidsModel model = purchaseBidsRepository.findByTransactionId(transactionId);
            model.setStockDetails(playBidRepository.findByBidId(model.getStockId()));
            return ResponseEntity.ok().header("Custom_Header_of_Product", "Get All Purchase Bids Successfully")
                    .body(model);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info " + e));
        }

    }


    @ApiOperation(value = "Search ByStockName Records", notes = "GET API to Fetch All Popular Products", response = List.class)
    @GetMapping(value = "/ByStockName/{Status}/{search}")
    public ResponseEntity<?> searchAllPurchaseBidsBy(@PathVariable String Status, @PathVariable String search,
             @PageableDefault(page = 0, size = 10, sort = "bidId", direction = Sort.Direction.DESC) Pageable pageable) {
        log.info("inside PurchaseBidsModel.ByStockName() method");

        Query query = new Query();
        List<Criteria> criteria = new ArrayList<Criteria>();
        criteria.add(Criteria.where("stockName").regex(search, "i"));
        criteria.add(Criteria.where("symbol").regex(search, "i"));
        if (Status.equals("All")) {

            if (search.matches("[0-9]+") && search.length() == 4) {
                query.addCriteria(Criteria.where("stockId").is(Long.parseLong(search)));

            } else {
                query.addCriteria(new Criteria().orOperator(criteria.toArray(new Criteria[criteria.size()])));
            }

        } else {


            if (search.matches("[0-9]+") && search.length() == 4) {
                query.addCriteria(Criteria.where("stockId").is(Long.parseLong(search)).
                        andOperator(Criteria.where("status").is(Status)));

            } else {
                query.addCriteria(Criteria.where("status").is(Status)
                        .andOperator(new Criteria().orOperator(criteria.toArray(new Criteria[criteria.size()]))));
            }
        }
        List<PurchaseBidsModel> model1 = mongoOperations.find(query, PurchaseBidsModel.class);


        if (model1.size() == 0) {

            return ResponseEntity.badRequest().header("Custom_Header_of_PlayBid", "search Not Found").body(model1);
        } else {
            Page<PurchaseBidsModel> model = PageableExecutionUtils.getPage(
                    model1,
                    pageable,
                    () -> mongoTemplate.count(query, PurchaseBidsModel.class));
            return ResponseEntity.ok().header("Custom_Header_of_Product", "Get All Popular Products Successfully")
                    .body(model);
        }


    }

    @ApiOperation(value = "Search ByStockName Records", notes = "GET API to Fetch All Popular Products", response = List.class)
    @GetMapping(value = "/ByStockName/{userId}/{Status}/{search}")
    public ResponseEntity<?> searchAllPurchaseBidsByUserId(@PathVariable String userId,@PathVariable String Status, @PathVariable String search,
                                                     @PageableDefault(page = 0, size = 10, sort = "bidId", direction = Sort.Direction.DESC) Pageable pageable) {
        log.info("inside PurchaseBidsModel.ByStockName() method");

        Query query = new Query();
        List<Criteria> criteria = new ArrayList<Criteria>();
        criteria.add(Criteria.where("userId").regex(search, "i"));
        criteria.add(Criteria.where("stockName").regex(search, "i"));
        criteria.add(Criteria.where("symbol").regex(search, "i"));
        if (Status.equals("All")) {

            if (search.matches("[0-9]+") && search.length() == 4) {
                query.addCriteria(Criteria.where("stockId").is(Long.parseLong(search)));

            } else {
                query.addCriteria(new Criteria().orOperator(criteria.toArray(new Criteria[criteria.size()])));
            }

        } else {


            if (search.matches("[0-9]+") && search.length() == 4) {
                query.addCriteria(Criteria.where("stockId").is(Long.parseLong(search)).
                        andOperator(Criteria.where("status").is(Status)));

            } else {
                query.addCriteria(Criteria.where("status").is(Status)
                        .andOperator(new Criteria().orOperator(criteria.toArray(new Criteria[criteria.size()]))));
            }
        }
        List<PurchaseBidsModel> model1 = mongoOperations.find(query, PurchaseBidsModel.class);


        if (model1.size() == 0) {

            return ResponseEntity.badRequest().header("Custom_Header_of_PlayBid", "search Not Found").body(model1);
        } else {
            Page<PurchaseBidsModel> model = PageableExecutionUtils.getPage(
                    model1,
                    pageable,
                    () -> mongoTemplate.count(query, PurchaseBidsModel.class));
            return ResponseEntity.ok().header("Custom_Header_of_Product", "Get All Popular Products Successfully")
                    .body(model);
        }


    }
}
