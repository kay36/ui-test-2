package com.vinbytes.binary.controller;

import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.PlayBidModel;
import com.vinbytes.binary.model.UserModel;
import com.vinbytes.binary.repository.UserRepository;
import com.vinbytes.binary.services.Twilio2FAService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)
@RestController
@RequestMapping(value="/twilio")
public class TwilioTwoFactorController extends Twilio2FAService {

    @Autowired
    public UserRepository userRepository;

    @PostMapping(value = "/get/user/{id}/{verifyVia}")
    public ResponseEntity<?> sendOtp(@PathVariable String id,  @PathVariable("verifyVia") String verifyVia) {
        try
        { UserModel userFrmDb=new UserModel();
            if (id.matches("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$") == true) {
                userFrmDb = userRepository.findByEmail(id);
            } else if (id.matches("[a-zA-Z0-9]+") == true) {
                userFrmDb = userRepository.findById(id).orElse(null);
            }

            if (userFrmDb != null) {
                boolean twilioMsgResponse = Twilio2FA(userFrmDb,verifyVia);
                if(twilioMsgResponse)
                {

                    return ResponseEntity.ok().body(new MessageResponse("OTP sent successfully"));
                }
                else
                {
                    System.out.println("error");
                    return ResponseEntity.badRequest().body(new MessageResponse("OTP not sent"));
                }
            }
            else
            {
                return ResponseEntity.badRequest().body(new MessageResponse("user not found"));
            }
        }catch (Exception e)
        {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info "+e.getMessage()));
        }


    }


    @PostMapping(value = "/get/user/verifyOtp/{id}/{code}/{verifyVia}")
    public ResponseEntity<?> verifiedotp(@PathVariable String id,@PathVariable String code,@PathVariable String verifyVia) {

        UserModel userFrmDb = userRepository.findById(id).orElse(null);
        if (userFrmDb != null) {
            boolean twilioOtpResponse = Twilio2FACodeCheck(userFrmDb,code,verifyVia);
            if(twilioOtpResponse)
            {
                return ResponseEntity.ok().body(new MessageResponse("verified successfully"));
            }
            else
            {
                return ResponseEntity.badRequest().body(new MessageResponse("OTP is invalid or expired"));
            }
        }
        else
        {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info "));
        }
    }

}
