package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.CoinBaseTransactionModal;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CoinBaseTransactionRepository extends MongoRepository<CoinBaseTransactionModal, String> {

    List<CoinBaseTransactionModal> findAllByReceivedPayment(boolean receivedPayment);

    CoinBaseTransactionModal findByCoinBaseTransactionId(String coinBaseTransactionId);
}
