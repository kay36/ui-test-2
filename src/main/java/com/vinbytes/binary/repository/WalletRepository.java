package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.WalletModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WalletRepository  extends MongoRepository<WalletModel, String> {

    WalletModel findByUserId(String userId);


    List<WalletModel> findTop10ByOrderByTotalProfitDesc();
    List<WalletModel> findTop5ByOrderByTotalProfitDesc();



}
