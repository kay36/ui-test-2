package com.vinbytes.binary.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.vinbytes.binary.model.VerifiedTokenModel;

@Repository
public interface VerifiedTokenRepository extends MongoRepository<VerifiedTokenModel, String> {

    VerifiedTokenModel findByToken(String token);

}

