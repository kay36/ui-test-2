package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.ContactModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ContactRepository extends MongoRepository<ContactModel, String> {
}
