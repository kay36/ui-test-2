package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.PlayBidModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayBidRepository extends MongoRepository<PlayBidModel, String> {
    boolean existsByBidId(String bidId);

    PlayBidModel findBystockName(String stockName);

    PlayBidModel findByBidId(String bidId);

    List<PlayBidModel> findAllByBidType(String bidType);

    List<PlayBidModel> findBySymbolAndBiddingStatus(String symbol, String biddingStatus);

    Page<PlayBidModel> findAllByBiddingStatus(String biddingStatus,Pageable pageable);

    List<PlayBidModel> findByBiddingStatusIn(List<PlayBidModel> data,String biddingStatus);

    List<PlayBidModel> findBySymbolOrStockName(String symbol,String stockName);

    Page<PlayBidModel> findBySymbolAndBiddingStatus(String symbol,String biddingStatus,Pageable pageable);

    List<PlayBidModel> findAll();

    List<PlayBidModel> findAllByBiddingStatus(String biddingStatus);

    Page<PlayBidModel> findAll(Pageable pageable);

    PlayBidModel findTop1ByBiddingStatusOrderByBiddingStartsAtAsc(String biddingStatus);
    PlayBidModel findTop1ByBiddingStatusOrderByBiddingClosesAtAsc(String biddingStatus);
    PlayBidModel findTop1ByBiddingStatusOrderByDrawTimeAsc(String biddingStatus);
    Long countByBiddingStatus(String biddingStatus);

    PlayBidModel findTop1ByBiddingStatusAndBidTypeOrderByBiddingClosesAtAsc(String biddingStatus, String bidType);

    PlayBidModel findTop1ByBiddingStatusAndBidTypeOrderByDrawTimeAsc(String biddingStatus, String bidType);
}
