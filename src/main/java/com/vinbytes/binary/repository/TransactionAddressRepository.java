package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.UserTransactionAddress;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TransactionAddressRepository extends MongoRepository<UserTransactionAddress, String> {

    public UserTransactionAddress findByUserId(String userId);


}
