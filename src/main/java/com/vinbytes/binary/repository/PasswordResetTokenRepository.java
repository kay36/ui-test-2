package com.vinbytes.binary.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.vinbytes.binary.model.PasswordResetTokenModel;

@Repository
public interface PasswordResetTokenRepository extends MongoRepository<PasswordResetTokenModel, String> {

    PasswordResetTokenModel findByToken(String token);

}
