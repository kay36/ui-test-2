package com.vinbytes.binary.repository;


import java.util.List;
import java.util.Optional;

import com.vinbytes.binary.model.PurchaseBidsModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.bson.types.ObjectId;
import com.vinbytes.binary.model.UserModel;

@Repository
public interface UserRepository extends MongoRepository<UserModel, String> {

	UserModel findById(ObjectId id);

	Optional<UserModel> findById(String id);

	UserModel findByusername(ObjectId username);

	Optional<UserModel> findByusername(String username);

	Boolean existsByusername(String username);

//	UserModel findByGoogleMailId (String googleMailId);
//
//	Boolean existsByGoogleMailId(String googleMailId);

	Boolean existsByEmail(String email);

	Boolean existsByPhoneNo(String phoneNo);

	Optional<UserModel> findByPhoneNoOrEmail(String phoneNo, String email);

	Boolean existsByPassword(String password);

	Boolean existsById(ObjectId id);

	Boolean existsByIdAndDeleted(ObjectId id,Boolean deleted);

	Boolean existsByEmailAndLoginType(String email,String loginType);

	List<UserModel> findAllByDeleted(boolean isDeleted);

	List<UserModel> findAll();

	Page<UserModel> findAll(Pageable pageable);

	UserModel findByEmail(String email);

	UserModel findByRefId(String refId);

	UserModel findByEmailIgnoreCase(String email);

	Optional<UserModel> findByIdAndTwilioId(String id, String twilioId);



}
