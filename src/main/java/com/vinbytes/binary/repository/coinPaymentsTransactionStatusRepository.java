package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.coinPaymentsTransactionStatusResponseModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface coinPaymentsTransactionStatusRepository extends MongoRepository<coinPaymentsTransactionStatusResponseModel, String> {

    coinPaymentsTransactionStatusResponseModel findByCoinPaymentsId(String coinPaymentsId);
}

