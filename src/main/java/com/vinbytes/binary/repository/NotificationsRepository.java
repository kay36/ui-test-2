package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.NotificationModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationsRepository extends MongoRepository<NotificationModel, String> {



    NotificationModel findByUserId(String userId);

    List<NotificationModel> findByUserIdAndNotificationDateFiltersNotificationPropertiesChecked(String userId,boolean Checked);

    List<NotificationModel> findTop15ByUserIdOrderByNotificationDateFiltersNotificationPropertiesDateAndTimeDesc(String userId);

}
