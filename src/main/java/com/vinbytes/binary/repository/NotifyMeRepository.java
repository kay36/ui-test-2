package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.NotifyMeModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotifyMeRepository extends MongoRepository<NotifyMeModel, String> {

    Boolean existsByEmail(String email);

    List<NotifyMeModel> findAll();

    Page<NotifyMeModel> findAll(Pageable pageable);
}