package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.PermissionModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionsRepository extends MongoRepository<PermissionModel, String> {

    PermissionModel findByUserId(String userId);
}
