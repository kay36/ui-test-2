package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.FavouriteModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FavouriteRepository extends MongoRepository<FavouriteModel, String> {

    FavouriteModel findByUserId(String userId);
}
