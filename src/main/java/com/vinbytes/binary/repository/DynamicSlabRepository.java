package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.DynamicSlabModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DynamicSlabRepository extends MongoRepository<DynamicSlabModel, String> {

    DynamicSlabModel findByType(String Type);
}
