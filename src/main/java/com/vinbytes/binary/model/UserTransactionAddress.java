package com.vinbytes.binary.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "UserTransactionAddress")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserTransactionAddress {

    @Id
    private String id;
    private String userId;
    private List<TransactionAddress> transactionAddress;
}
