package com.vinbytes.binary.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("PasswordResetToken Model Details")
@Document(collection = "password-reset-token")
public class PasswordResetTokenModel implements Serializable {

    @Id
    @ApiModelProperty(required = false, hidden = true)
    private String id;

    @ApiModelProperty(value = "Token of PasswordResetToken Model", required = true)
    private String token;

    @DBRef
    @ApiModelProperty(value = "UserModel of PasswordResetToken Model", required = true)
    private UserModel user;

    @ApiModelProperty(value = "Date of PasswordResetToken Model", required = true)
    private Date expiryDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void setExpiryDate(int minutes){
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE, minutes);
        this.expiryDate = now.getTime();
    }

    public boolean isExpired() {
        return new Date().after(this.expiryDate);
    }
}