package com.vinbytes.binary.model;

public class addMoneyRequestModel {

    private String userId;
    private Double amount;
    private String amountType;

    public addMoneyRequestModel() {
    }

    public addMoneyRequestModel(String userId, Double amount, String amountType) {
        this.userId = userId;
        this.amount = amount;
        this.amountType = amountType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }
}
