package com.vinbytes.binary.model;

public class HelloMessages {
    private String name;

    public HelloMessages() {
    }

    public HelloMessages(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}