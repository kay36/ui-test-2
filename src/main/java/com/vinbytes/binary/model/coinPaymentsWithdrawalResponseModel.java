package com.vinbytes.binary.model;

public class coinPaymentsWithdrawalResponseModel {

    private String id;
    private String status;
    private Double amount;

    public coinPaymentsWithdrawalResponseModel() {
    }

    public coinPaymentsWithdrawalResponseModel(String id, String status, Double amount) {
        this.id = id;
        this.status = status;
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
