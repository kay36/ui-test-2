package com.vinbytes.binary.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import org.springframework.stereotype.Component;
import java.util.Date;
import java.util.List;

@Component
@Document(collection = "playbid")
public class PlayBidModel {

    @Id
    private String id;
    private String bidId;
    private String stockName;
    private List<String> userId;
    private Date createdOn;
    private Date biddingStartsAt;
    private Date biddingClosesAt;
    private String changeinStockValue;
    private Date drawTime;
    private String bidType;
    private String stockValue;
    private String valueOfOneBid;
    private String noOfBidders;
    private String biddingStatus;
    private String symbol;
    private String imageUrl;
    private DynamicSlabModel dynamicSlabDetails;
    private Double closedTimeResults;
    private Double drawTimeResults;
    private Double totalRevenue;
    private Double totalWinningAmount;
    private Double totalAmountBid;
    private Double totalTransactionFees;
    private Double totalNoOfBiddersWon;
    private Double totalNoOfBidsPurchased;
    private Double totalNoOfBiddersLoss;
    private List<Double> totalNoOfAmountInSlab;
    private List<String> wonUserId;
    private String winningSlab;
    private String winningLevel;
    private List<LiveBidders> liveBidders;
    private List<RangeCountModel> rangeCountModels;
    private Date biddingClosedAtTime;

    public PlayBidModel(){

    }

    public PlayBidModel(String id, String bidId, String stockName, List<String> userId, Date createdOn, Date biddingStartsAt, Date biddingClosesAt, String changeinStockValue, Date drawTime, String bidType, String stockValue, String valueOfOneBid, String noOfBidders, String biddingStatus, String symbol, String imageUrl, DynamicSlabModel dynamicSlabDetails, Double closedTimeResults, Double drawTimeResults, Double totalRevenue, Double totalWinningAmount, Double totalAmountBid, Double totalTransactionFees, Double totalNoOfBiddersWon, Double totalNoOfBidsPurchased, Double totalNoOfBiddersLoss, List<Double> totalNoOfAmountInSlab, List<String> wonUserId, String winningSlab, String winningLevel, List<LiveBidders> liveBidders, List<RangeCountModel> rangeCountModels, Date biddingClosedAtTime) {
        this.id = id;
        this.bidId = bidId;
        this.stockName = stockName;
        this.userId = userId;
        this.createdOn = createdOn;
        this.biddingStartsAt = biddingStartsAt;
        this.biddingClosesAt = biddingClosesAt;
        this.changeinStockValue = changeinStockValue;
        this.drawTime = drawTime;
        this.bidType = bidType;
        this.stockValue = stockValue;
        this.valueOfOneBid = valueOfOneBid;
        this.noOfBidders = noOfBidders;
        this.biddingStatus = biddingStatus;
        this.symbol = symbol;
        this.imageUrl = imageUrl;
        this.dynamicSlabDetails = dynamicSlabDetails;
        this.closedTimeResults = closedTimeResults;
        this.drawTimeResults = drawTimeResults;
        this.totalRevenue = totalRevenue;
        this.totalWinningAmount = totalWinningAmount;
        this.totalAmountBid = totalAmountBid;
        this.totalTransactionFees = totalTransactionFees;
        this.totalNoOfBiddersWon = totalNoOfBiddersWon;
        this.totalNoOfBidsPurchased = totalNoOfBidsPurchased;
        this.totalNoOfBiddersLoss = totalNoOfBiddersLoss;
        this.totalNoOfAmountInSlab = totalNoOfAmountInSlab;
        this.wonUserId = wonUserId;
        this.winningSlab = winningSlab;
        this.winningLevel = winningLevel;
        this.liveBidders = liveBidders;
        this.rangeCountModels = rangeCountModels;
        this.biddingClosedAtTime = biddingClosedAtTime;
    }

    public Date getBiddingClosedAtTime() {
        return biddingClosedAtTime;
    }

    public void setBiddingClosedAtTime(Date biddingClosedAtTime) {
        this.biddingClosedAtTime = biddingClosedAtTime;
    }

    public PlayBidModel(String stockName, Date createdOn, Date biddingStartsAt, Date biddingClosesAt, Date drawTime, String bidType, String valueOfOneBid, String biddingStatus, String symbol, String imageUrl) {
        this.stockName = stockName;
        this.createdOn = createdOn;
        this.biddingStartsAt = biddingStartsAt;
        this.biddingClosesAt = biddingClosesAt;
        this.drawTime = drawTime;
        this.bidType = bidType;
        this.valueOfOneBid = valueOfOneBid;
        this.biddingStatus=biddingStatus;
        this.symbol = symbol;
        this.imageUrl = imageUrl;
    }

    public List<RangeCountModel> getRangeCountModels() {
        return rangeCountModels;
    }

    public void setRangeCountModels(List<RangeCountModel> rangeCountModels) {
        this.rangeCountModels = rangeCountModels;
    }

    public List<LiveBidders> getLiveBidders() {
        return liveBidders;
    }

    public void setLiveBidders(List<LiveBidders> liveBidders) {
        this.liveBidders = liveBidders;
    }

    public String getWinningLevel() {
        return winningLevel;
    }

    public void setWinningLevel(String winningLevel) {
        this.winningLevel = winningLevel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBidId() {
        return bidId;
    }

    public List<String> getWonUserId() {
        return wonUserId;
    }

    public void setWonUserId(List<String> wonUserId) {
        this.wonUserId = wonUserId;
    }

    public void setBidId(String bidId) {
        this.bidId = bidId;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public List<String> getUserId() {
        return userId;
    }

    public void setUserId(List<String> userId) {
        this.userId = userId;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getBiddingStartsAt() {
        return biddingStartsAt;
    }

    public void setBiddingStartsAt(Date biddingStartsAt) {
        this.biddingStartsAt = biddingStartsAt;
    }

    public Date getBiddingClosesAt() {
        return biddingClosesAt;
    }

    public void setBiddingClosesAt(Date biddingClosesAt) {
        this.biddingClosesAt = biddingClosesAt;
    }

    public String getChangeinStockValue() {
        return changeinStockValue;
    }

    public void setChangeinStockValue(String changeinStockValue) {
        this.changeinStockValue = changeinStockValue;
    }

    public Date getDrawTime() {
        return drawTime;
    }

    public void setDrawTime(Date drawTime) {
        this.drawTime = drawTime;
    }

    public String getBidType() {
        return bidType;
    }

    public void setBidType(String bidType) {
        this.bidType = bidType;
    }

    public String getStockValue() {
        return stockValue;
    }

    public void setStockValue(String stockValue) {
        this.stockValue = stockValue;
    }

    public String getValueOfOneBid() {
        return valueOfOneBid;
    }

    public void setValueOfOneBid(String valueOfOneBid) {
        this.valueOfOneBid = valueOfOneBid;
    }

    public String getNoOfBidders() {
        return noOfBidders;
    }

    public void setNoOfBidders(String noOfBidders) {
        this.noOfBidders = noOfBidders;
    }

    public String getBiddingStatus() {
        return biddingStatus;
    }

    public void setBiddingStatus(String biddingStatus) {
        this.biddingStatus = biddingStatus;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Double getClosedTimeResults() {
        return closedTimeResults;
    }

    public void setClosedTimeResults(Double closedTimeResults) {
        this.closedTimeResults = closedTimeResults;
    }

    public Double getDrawTimeResults() {
        return drawTimeResults;
    }

    public void setDrawTimeResults(Double drawTimeResults) {
        this.drawTimeResults = drawTimeResults;
    }

    public Double getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(Double totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public Double getTotalWinningAmount() {
        return totalWinningAmount;
    }

    public void setTotalWinningAmount(Double totalWinningAmount) {
        this.totalWinningAmount = totalWinningAmount;
    }

    public Double getTotalAmountBid() {
        return totalAmountBid;
    }

    public void setTotalAmountBid(Double totalAmountBid) {
        this.totalAmountBid = totalAmountBid;
    }

    public Double getTotalTransactionFees() {
        return totalTransactionFees;
    }

    public void setTotalTransactionFees(Double totalTransactionFees) {
        this.totalTransactionFees = totalTransactionFees;
    }

    public Double getTotalNoOfBiddersWon() {
        return totalNoOfBiddersWon;
    }

    public void setTotalNoOfBiddersWon(Double totalNoOfBiddersWon) {
        this.totalNoOfBiddersWon = totalNoOfBiddersWon;
    }

    public Double getTotalNoOfBidsPurchased() {
        return totalNoOfBidsPurchased;
    }

    public void setTotalNoOfBidsPurchased(Double totalNoOfBidsPurchased) {
        this.totalNoOfBidsPurchased = totalNoOfBidsPurchased;
    }

    public Double getTotalNoOfBiddersLoss() {
        return totalNoOfBiddersLoss;
    }

    public void setTotalNoOfBiddersLoss(Double totalNoOfBiddersLoss) {
        this.totalNoOfBiddersLoss = totalNoOfBiddersLoss;
    }

    public List<Double> getTotalNoOfAmountInSlab() {
        return totalNoOfAmountInSlab;
    }

    public void setTotalNoOfAmountInSlab(List<Double> totalNoOfAmountInSlab) {
        this.totalNoOfAmountInSlab = totalNoOfAmountInSlab;
    }

    public DynamicSlabModel getDynamicSlabDetails() {
        return dynamicSlabDetails;
    }

    public void setDynamicSlabDetails(DynamicSlabModel dynamicSlabDetails) {
        this.dynamicSlabDetails = dynamicSlabDetails;
    }

    public String getWinningSlab() {
        return winningSlab;
    }

    public void setWinningSlab(String winningSlab) {
        this.winningSlab = winningSlab;
    }

    @Override
    public String toString() {
        return "PlayBidModel{" +
                "id='" + id + '\'' +
                ", bidId='" + bidId + '\'' +
                ", stockName='" + stockName + '\'' +
                ", userId=" + userId +
                ", createdOn=" + createdOn +
                ", biddingStartsAt=" + biddingStartsAt +
                ", biddingClosesAt=" + biddingClosesAt +
                ", changeinStockValue='" + changeinStockValue + '\'' +
                ", drawTime=" + drawTime +
                ", bidType='" + bidType + '\'' +
                ", stockValue='" + stockValue + '\'' +
                ", valueOfOneBid='" + valueOfOneBid + '\'' +
                ", noOfBidders='" + noOfBidders + '\'' +
                ", biddingStatus='" + biddingStatus + '\'' +
                ", symbol='" + symbol + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", dynamicSlabDetails=" + dynamicSlabDetails +
                ", closedTimeResults=" + closedTimeResults +
                ", drawTimeResults=" + drawTimeResults +
                ", totalRevenue=" + totalRevenue +
                ", totalWinningAmount=" + totalWinningAmount +
                ", totalAmountBid=" + totalAmountBid +
                ", totalTransactionFees=" + totalTransactionFees +
                ", totalNoOfBiddersWon=" + totalNoOfBiddersWon +
                ", totalNoOfBidsPurchased=" + totalNoOfBidsPurchased +
                ", totalNoOfBiddersLoss=" + totalNoOfBiddersLoss +
                ", totalNoOfAmountInSlab=" + totalNoOfAmountInSlab +
                ", wonUserId=" + wonUserId +
                '}';
    }
}