package com.vinbytes.binary.model;

import java.util.Date;

public class transactionStatusModel {

    private Date transactionDateAndTime;
    private String transactionStatus;
    private Double receivedAmount = 0.00;

    public transactionStatusModel() {
    }

    public transactionStatusModel(Date transactionDateAndTime, String transactionStatus, Double receivedAmount) {
        this.transactionDateAndTime = transactionDateAndTime;
        this.transactionStatus = transactionStatus;
        this.receivedAmount = receivedAmount;
    }

    public Date getTransactionDateAndTime() {
        return transactionDateAndTime;
    }

    public void setTransactionDateAndTime(Date transactionDateAndTime) {
        this.transactionDateAndTime = transactionDateAndTime;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public Double getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(Double receivedAmount) {
        this.receivedAmount = receivedAmount;
    }
}
