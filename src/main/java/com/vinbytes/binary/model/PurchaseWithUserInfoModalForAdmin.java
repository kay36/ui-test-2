package com.vinbytes.binary.model;

import org.springframework.data.annotation.Id;

public class PurchaseWithUserInfoModalForAdmin {

    private PurchaseBidsModel purchase;
    private UserModel userModel;
    private TransactionModal transactionModal;

    public PurchaseWithUserInfoModalForAdmin() {
    }

    public PurchaseWithUserInfoModalForAdmin(PurchaseBidsModel purchase, UserModel userModel, TransactionModal transactionModal) {
        this.purchase = purchase;
        this.userModel = userModel;
        this.transactionModal = transactionModal;
    }

    public PurchaseBidsModel getPurchase() {
        return purchase;
    }

    public void setPurchase(PurchaseBidsModel purchase) {
        this.purchase = purchase;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public TransactionModal getTransactionModal() {
        return transactionModal;
    }

    public void setTransactionModal(TransactionModal transactionModal) {
        this.transactionModal = transactionModal;
    }
}
