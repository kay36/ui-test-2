package com.vinbytes.binary.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@ApiModel(description = "User Wallet")
@Document(collection = "Wallet")
public class WalletModel {

    @Id
    @ApiModelProperty(value = "Wallet id", required = true)
    private String id;

    @ApiModelProperty(value = "Wallet User id", required = true)
    private String userId;

    @ApiModelProperty(value = "dollars", required = true)
    private Double dollars = 0.00;

    @ApiModelProperty(value = "profit", required = true)
    private Double totalProfit = 0.00;

    @ApiModelProperty(value = "loss", required = true)
    private Double totalLoss = 0.00;


    @ApiModelProperty(value = "profit", required = true)
    private Double totalNumberOfProfit = 0.00;

    @ApiModelProperty(value = "loss", required = true)
    private Double totalNumberOfLoss = 0.00;

    @ApiModelProperty(value = "credit", required = true)
    private Double totalCredit = 0.00;

    @ApiModelProperty(value = "debit", required = true)
    private Double totalDebit = 0.00;

    @ApiModelProperty(value = "creditCount", required = true)
    private Double totalNumberOfTimesCredit = 0.00;

    @ApiModelProperty(value = "debitCount", required = true)
    private Double totalNumberOfTimesDebit = 0.00;

    @ApiModelProperty(value = "bitAmount", required = true)
    private Double totalBitAmount = 0.00;

    @ApiModelProperty(value = "bitCount", required = true)
    private Double totalNumberOfBitsPurchased = 0.00;

    @ApiModelProperty(value = "transactionFee", required = true)
    private Double totalTransactionFee = 0.00;

    @ApiModelProperty(value = "RewardWallet", required = true)
    private Double rewardWallet = 0.00;

    public WalletModel() {
    }

    public WalletModel(String id, String userId, Double dollars, Double totalProfit, Double totalLoss, Double totalNumberOfProfit, Double totalNumberOfLoss, Double totalCredit, Double totalDebit, Double totalNumberOfTimesCredit, Double totalNumberOfTimesDebit, Double totalBitAmount, Double totalNumberOfBitsPurchased, Double totalTransactionFee, Double rewardWallet) {
        this.id = id;
        this.userId = userId;
        this.dollars = dollars;
        this.totalProfit = totalProfit;
        this.totalLoss = totalLoss;
        this.totalNumberOfProfit = totalNumberOfProfit;
        this.totalNumberOfLoss = totalNumberOfLoss;
        this.totalCredit = totalCredit;
        this.totalDebit = totalDebit;
        this.totalNumberOfTimesCredit = totalNumberOfTimesCredit;
        this.totalNumberOfTimesDebit = totalNumberOfTimesDebit;
        this.totalBitAmount = totalBitAmount;
        this.totalNumberOfBitsPurchased = totalNumberOfBitsPurchased;
        this.totalTransactionFee = totalTransactionFee;
        this.rewardWallet = rewardWallet;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Double getDollars() {
        return dollars;
    }

    public void setDollars(Double dollars) {
        this.dollars = dollars;
    }

    public Double getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(Double totalProfit) {
        this.totalProfit = totalProfit;
    }

    public Double getTotalLoss() {
        return totalLoss;
    }

    public void setTotalLoss(Double totalLoss) {
        this.totalLoss = totalLoss;
    }

    public Double getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(Double totalCredit) {
        this.totalCredit = totalCredit;
    }

    public Double getTotalDebit() {
        return totalDebit;
    }

    public void setTotalDebit(Double totalDebit) {
        this.totalDebit = totalDebit;
    }

    public Double getTotalNumberOfTimesCredit() {
        return totalNumberOfTimesCredit;
    }

    public void setTotalNumberOfTimesCredit(Double totalNumberOfTimesCredit) {
        this.totalNumberOfTimesCredit = totalNumberOfTimesCredit;
    }

    public Double getTotalNumberOfTimesDebit() {
        return totalNumberOfTimesDebit;
    }

    public void setTotalNumberOfTimesDebit(Double totalNumberOfTimesDebit) {
        this.totalNumberOfTimesDebit = totalNumberOfTimesDebit;
    }

    public Double getTotalBitAmount() {
        return totalBitAmount;
    }

    public void setTotalBitAmount(Double totalBitAmount) {
        this.totalBitAmount = totalBitAmount;
    }

    public Double getTotalNumberOfBitsPurchased() {
        return totalNumberOfBitsPurchased;
    }

    public void setTotalNumberOfBitsPurchased(Double totalNumberOfBitsPurchased) {
        this.totalNumberOfBitsPurchased = totalNumberOfBitsPurchased;
    }

    public Double getTotalTransactionFee() {
        return totalTransactionFee;
    }

    public void setTotalTransactionFee(Double totalTransactionFee) {
        this.totalTransactionFee = totalTransactionFee;
    }

    public Double getTotalNumberOfProfit() {
        return totalNumberOfProfit;
    }

    public void setTotalNumberOfProfit(Double totalNumberOfProfit) {
        this.totalNumberOfProfit = totalNumberOfProfit;
    }

    public Double getTotalNumberOfLoss() {
        return totalNumberOfLoss;
    }

    public void setTotalNumberOfLoss(Double totalNumberOfLoss) {
        this.totalNumberOfLoss = totalNumberOfLoss;
    }

    public Double getRewardWallet() {
        return rewardWallet;
    }

    public void setRewardWallet(Double rewardWallet) {
        this.rewardWallet = rewardWallet;
    }
}
