package com.vinbytes.binary.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class PermissionCategoryModel {

    @ApiModelProperty(value = "Category Name", required = true)
    public String category;
    @ApiModelProperty(value = "List of Permission SubCategory Model", required = true)
    public List<PermissionSubCategoryModel> subCategory;

    public PermissionCategoryModel() {
    }

    public PermissionCategoryModel(String category, List<PermissionSubCategoryModel> subCategory) {
        this.category = category;
        this.subCategory = subCategory;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<PermissionSubCategoryModel> getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(List<PermissionSubCategoryModel> subCategory) {
        this.subCategory = subCategory;
    }
}
