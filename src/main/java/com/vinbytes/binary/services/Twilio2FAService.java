package com.vinbytes.binary.services;

import com.twilio.Twilio;
import com.twilio.rest.verify.v2.service.Verification;
import com.twilio.rest.verify.v2.service.VerificationCheck;
import com.vinbytes.binary.model.*;
import com.vinbytes.binary.repository.Twilio2FARepository;
import com.vinbytes.binary.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class Twilio2FAService {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private Twilio2FARepository twilio2FARepository;

    public static final String ACCOUNT_SID = "AC7eaa335eb520360d4c1f11ba1026fbf8";
    public static final String AUTH_TOKEN = "065f25014850c2f7517aaa9e864d47fe";
    public static final String PATH_SERVICE_ID = "VAc9eacce469bfa0338e798f60e465d319";

    private String requestUrl = "https://verify.twilio.com/v2/Services/VAc9eacce469bfa0338e798f60e465d319";
    private String authorizationToken = "Basic QUM3ZWFhMzM1ZWI1MjAzNjBkNGMxZjExYmExMDI2ZmJmODowNjVmMjUwMTQ4NTBjMmY3NTE3YWFhOWU4NjRkNDdmZQ==";

    private static final Logger log = LoggerFactory.getLogger(Twilio2FAService.class);

    public boolean Twilio2FA(UserModel userModel, String verifyVia){
        try{
              if (userModel != null){
                UserModel userFromDB = userRepository.findById(userModel.getId()).orElse(null);
                if (userFromDB!=null){
                    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
                    String to;
                    String channel;
                    if(verifyVia.equals("phone")){
                        to = "+"+userFromDB.getCountryCode()+userFromDB.getPhoneNo();
                        channel = "sms";
                    }else if (verifyVia.equals("email")){
                        to = userFromDB.getEmail();
                        channel = "email";
                    }else {
                        log.error("Invalid, Twilio verify Via");
                        return false;
                    }
                    Verification verification = Verification.creator(
                            PATH_SERVICE_ID,
                            to,
                            channel).create();


                  if (verification!=null && verification.getSid()!=null){
                        userFromDB.setTwilioId(verification.getSid());
                        userRepository.save(userFromDB);
                        return true;
                    }else {
                        log.error("Twilio Request Error !");
                        return false;
                    }
                }else {
                    log.error("User Not Found !");
                    return false;
                }
            }else {
                log.error("User Not Found !");
                return false;
            }
        }catch (Exception e){
            log.error("unhandled Error Occurred : " + e.getMessage());
            return false;
        }
    }


    /*public boolean Twilio2FAStatus(UserModel userModel){
        try{
            if (userModel != null){
                UserModel userFromDB = userRepository.findByIdAndTwilioId(userModel.getId(), userModel.getTwilioId()).orElse(null);
                if (userFromDB!=null){
                    RestTemplate restTemplate = new RestTemplate();

                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
                    headers.add("Authorization", authorizationToken);

                    String twilioId = userFromDB.getTwilioId();
                    ResponseEntity<TwilioOTPResponseModal> twilioOTPResponseModal = restTemplate.exchange(requestUrl+"/Verifications/"+twilioId, HttpMethod.GET, null, TwilioOTPResponseModal.class);
                    if (twilioOTPResponseModal.getBody()!=null && twilioOTPResponseModal.getBody().getSid()!=null && twilioOTPResponseModal.getBody().getSid().equals(twilioId)){
                        Twilio2FAModal twilio2FAModal = twilio2FARepository.findByUserIdAndTwilioId(userFromDB.getId(), userFromDB.getTwilioId());
                        List<TwilioOTPResponseModal> twilioOTPResponseModalList = new ArrayList<>();
                        if (twilio2FAModal.getTwilioOTPResponseModals().size()>0){
                            twilioOTPResponseModalList = twilio2FAModal.getTwilioOTPResponseModals();
                        }
                        twilioOTPResponseModalList.add(twilioOTPResponseModal.getBody());
                        twilio2FAModal.setTwilioOTPResponseModals(twilioOTPResponseModalList);
                        twilio2FARepository.save(twilio2FAModal);
                        boolean status = false;
                        if (twilioOTPResponseModal.getBody().getStatus().equals("approved") && twilioOTPResponseModal.getBody().isValid()){
                            userFromDB.setTwilioId(null);
                            status = true;
                        }
                        userRepository.save(userFromDB);
                        return status;
                    }else {
                        log.error("Twilio Request Error !");
                        return false;
                    }
                }else {
                    log.error("User Not Found !");
                    return false;
                }
            }else {
                log.error("User Not Found !");
                return false;
            }
        }catch (Exception e){
            log.error("unhandled Error Occurred : " + e.getMessage());
            return false;
        }
    }
*/

    public boolean Twilio2FACodeCheck(UserModel userModel, String code, String verifyVia){
            try{
            if (userModel != null){
                UserModel userFromDB = userRepository.findByIdAndTwilioId(userModel.getId(), userModel.getTwilioId()).orElse(null);
                if (userFromDB!=null){
                    String to;
                    if(verifyVia.equals("phone")){
                        to = "+"+userFromDB.getCountryCode()+userFromDB.getPhoneNo();
                    }else if (verifyVia.equals("email")){
                        to = userFromDB.getEmail();
                    }else {
                        log.error("Invalid, Twilio verify Via");
                        return false;
                    }
                    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

                    VerificationCheck verificationCheck = VerificationCheck.creator(
                            PATH_SERVICE_ID,
                            code)
                            .setTo(to).create();

                    if (verificationCheck!=null && verificationCheck.getSid()!=null && verificationCheck.getSid().equals(userFromDB.getTwilioId())){
                        boolean status = false;
                        if (verificationCheck.getStatus().equals("approved")){
                            userFromDB.setTwilioId(null);
                            status = true;
                        }
                        userRepository.save(userFromDB);
                        return status;
                    }else {
                        log.error("Twilio Request Error !");
                        return false;
                    }
                }else {
                    log.error("User Not Found !");
                    return false;
                }
            }else {
                log.error("User Not Found !");
                return false;
            }
        }catch (Exception e){
            log.error("unhandled Error Occurred : " + e.getMessage());
            return false;
        }
    }
}
