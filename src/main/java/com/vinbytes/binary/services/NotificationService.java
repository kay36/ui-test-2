package com.vinbytes.binary.services;

import com.vinbytes.binary.model.NotificationDateFilter;
import com.vinbytes.binary.model.NotificationModel;
import com.vinbytes.binary.model.NotificationProperties;
import com.vinbytes.binary.repository.NotificationsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class NotificationService {

    @Autowired
    private NotificationsRepository notificationsRepository;

    private static final Logger log = LoggerFactory.getLogger(NotificationService.class);

    public Boolean MakeNotifications(String userId, String title, String description, String image, String url, String transactionId){
        log.info("inside NotificationService.MakeNotifications() method");
        try{
            Date dateAndTime = new Date();
            Date dateAndTimeFilter = new Date();
            NotificationModel newUserNotificationModel = new NotificationModel();
            newUserNotificationModel.setUserId(userId);
            NotificationDateFilter newUserNotificationDateFilter = new NotificationDateFilter();
            newUserNotificationDateFilter.setDate(dateAndTimeFilter);
            NotificationProperties newUserNotificationProperties = new NotificationProperties();
            newUserNotificationProperties.setDateAndTime(dateAndTime);
            newUserNotificationProperties.setTitle(title);
            newUserNotificationProperties.setDescription(description);
            newUserNotificationProperties.setImage(image);
            newUserNotificationProperties.setUrl(url);
            newUserNotificationProperties.setTransactionId(transactionId);
            newUserNotificationDateFilter.setNotificationProperties(Collections.singletonList(newUserNotificationProperties));
            newUserNotificationModel.setNotificationDateFilters(Collections.singletonList(newUserNotificationDateFilter));
            notificationsRepository.save(newUserNotificationModel);
            return true;
        }catch (Exception e){
            log.info("Unexpected Error !! - " + e);
            return false;
        }
    }
}
