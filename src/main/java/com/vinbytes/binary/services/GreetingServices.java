package com.vinbytes.binary.services;

import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.*;
import com.vinbytes.binary.repository.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class GreetingServices {

    @Autowired
    private PlayBidRepository playBidRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private NotificationsRepository notificationsRepository;


    @Autowired
    private DynamicSlabRepository dynamicSlabRepository;


    @Autowired
    private PurchaseBidsRepository purchaseBidsRepository;

    private final SimpMessagingTemplate simpMessagingTemplate;
    private static final String WS_MESSAGE_TRANSFER_DESTINATION = "/topic/onLiveBidsReceived";
    private List<String> userNames = new ArrayList<>();



    GreetingServices(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }


    public void sendMessages() {

        List<PlayBidModel> frmDb = playBidRepository.findAllByBiddingStatus("Open Now");
        for (PlayBidModel playBidModel : frmDb) {
        List<LiveBidders> LiveBidders1=new ArrayList<>();
        List<RangeCountModel> rangeCountModels=  getBidsCountWithRange(playBidModel);
          if(rangeCountModels!=null)
          {
              playBidModel.setRangeCountModels(rangeCountModels);
          }
            if (playBidModel!=null && playBidModel.getUserId()!=null &&playBidModel.getUserId().size()>0) {
            for (String id : playBidModel.getUserId()) {
            if (id != null) {
            UserModel userFrmDb = userRepository.findById(id).orElse(null);
            LiveBidders liveBidders = new LiveBidders(userFrmDb.getUsername(), userFrmDb.getProfileUrl());
            LiveBidders1.add(liveBidders);
            }
            }
            playBidModel.setLiveBidders(LiveBidders1);
            }
            }
          simpMessagingTemplate.convertAndSend(WS_MESSAGE_TRANSFER_DESTINATION,frmDb);
    }



    public void sendWallet() {
        for (String userName : userNames) {
            UserModel frmDb = userRepository.findByRefId(userName);
            if(frmDb!=null)
            {
                WalletModel walletfrmDb = walletRepository.findByUserId(frmDb.getId());
                simpMessagingTemplate.convertAndSendToUser(userName, "/topic/user-wallet", walletfrmDb);
            }
        }
    }




    public void sendNotification() {

        for (String userName : userNames) {
            UserModel frmDb = userRepository.findByRefId(userName);

            if(frmDb!=null)
            {
                   List<NotificationModel >notification = notificationsRepository.findTop15ByUserIdOrderByNotificationDateFiltersNotificationPropertiesDateAndTimeDesc(frmDb.getId());

                   if(notification!=null)
                   {
                       simpMessagingTemplate.convertAndSendToUser(userName, "/topic/notification", notification);
                   }



            }
        }

    }


    public void addUserName(String username) {

        userNames.add(username);
    }

    public void getTop5LeaderBoard(){
        List<WalletModel> model = walletRepository.findTop5ByOrderByTotalProfitDesc();
        if (model != null){
            for(WalletModel singleWalletModel : model){
                String id = singleWalletModel.getUserId();
                UserModel userModel = userRepository.findById(id).orElse(null);
                if (userModel != null){
                    singleWalletModel.setId(userModel.getUsername());
                    singleWalletModel.setUserId(userModel.getProfileUrl());
                }
            }
            simpMessagingTemplate.convertAndSend("/topic/leaderBoard",model);

        }
    }





    public void GetTop3HighestBidders() {
        try {

            List<TransactionModal> transactionModal = transactionRepository.findTop3ByTransactionTypeOrderByBidAmountDesc("Bid Purchased");
            List<LiveBidders> LiveBidders1=new ArrayList<>();
            if (transactionModal != null){
                for (TransactionModal singleTransactionModal : transactionModal){
                    String id = singleTransactionModal.getUserId();
                    UserModel userModel = userRepository.findById(id).orElse(null);
                    if (userModel != null){
                        LiveBidders liveBidders = new LiveBidders(userModel.getUsername(), userModel.getProfileUrl());
                        LiveBidders1.add(liveBidders);

                    }
                }
                simpMessagingTemplate.convertAndSend("/topic/maxAmtBidders",LiveBidders1);

            }
        } catch (Exception e) {
            System.out.println("Error info " + e.getMessage());
        }
    }




    public void GetTop3HighestWonBidders() {
        try {

            List<TransactionModal> transactionModal = transactionRepository.findTop3ByTransactionTypeOrderByBidAmountDesc("Winning Amount");
            List<LiveBidders> LiveBidders1=new ArrayList<>();
            if (transactionModal != null){
                for (TransactionModal singleTransactionModal : transactionModal){
                    String id = singleTransactionModal.getUserId();
                    UserModel userModel = userRepository.findById(id).orElse(null);
                    if (userModel != null){
                        LiveBidders liveBidders = new LiveBidders(userModel.getUsername(), userModel.getProfileUrl());
                        LiveBidders1.add(liveBidders);
                      /*  singleTransactionModal.setStockName(userModel.getUsername());
                        singleTransactionModal.setStockSymbol(userModel.getProfileUrl());*/
                    }
                }
                simpMessagingTemplate.convertAndSend("/topic/topWonBidders",LiveBidders1);

            }
        } catch (Exception e) {
            System.out.println("Error info " + e.getMessage());
        }
    }


    public List<RangeCountModel>  getBidsCountWithRange(PlayBidModel playBidModel) {

        if (playBidModel == null) {
            return null;
        } else {
            try {
                List<RangeCountModel> rangeCountModels = new ArrayList<>();
                DynamicSlabModel dynamicSlabModel = dynamicSlabRepository.findByType(playBidModel.getBidType());
                if (dynamicSlabModel == null){
                    return null;
                }else {
                    List<DynamicSlabValues> dynamicSlabValuesList = dynamicSlabModel.getDynamicSlabValues();
                    List<String> ranges = new ArrayList<>();
                    for (DynamicSlabValues dynamicSlabValues : dynamicSlabValuesList){
                        long fromValue = Math.round(dynamicSlabValues.getFromValue());
                        long toValue = Math.round(dynamicSlabValues.getToValue());
                        if (fromValue == toValue){
                            ranges.add("over $"+fromValue);
                        }else {
                            ranges.add("$"+fromValue+" - $"+toValue);
                        }
                    }

                    for (String rangeString : ranges) {
                        RangeCountModel rangeCount = new RangeCountModel();
                        rangeCount.setRange(rangeString);
                        List<PurchaseBidsModel> purchaseBidsModelsUp = purchaseBidsRepository.findAllByStockIdAndLevelAndChangesBtwAndPurchaseStatus(playBidModel.getBidId(), "Up", rangeString,"Success");
                        List<PurchaseBidsModel> purchaseBidsModelsDown = purchaseBidsRepository.findAllByStockIdAndLevelAndChangesBtwAndPurchaseStatus(playBidModel.getBidId(), "Down", rangeString,"Success");
                        rangeCount.setUp(String.valueOf(purchaseBidsModelsUp.size()));
                        rangeCount.setDown(String.valueOf(purchaseBidsModelsDown.size()));
                        rangeCountModels.add(rangeCount);
                    }


                        return rangeCountModels;

                }
            } catch (Exception e) {
                System.out.println("Greeting service"+ e.getMessage());
                return  null;
            }
        }

    }

}
