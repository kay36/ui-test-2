package com.vinbytes.binary.services;

import org.springframework.stereotype.Service;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Service
public class AmazonSimpleMailService {

    static final String FROM = "no-reply@play-crypto.com";
    static final String FROMNAME = "Play Crypto";

    static final String SMTP_USERNAME = "AKIA4MBZVSZK2GOEWONE";
    static final String SMTP_PASSWORD = "BLrT7KdeBn8poxqq2YJ8rw0MS+8Uv/EQ7/GcByht+uf2";

    static final String CONFIGSET = "ConfigSet";

    static final String HOST = "email-smtp.us-east-1.amazonaws.com";
    static final int PORT = 587;

    /*static final String SUBJECT = "Amazon SES test (SMTP interface accessed using Java)";

    static final String BODY = String.join(
            System.getProperty("line.separator"),
            "<h1>Amazon SES SMTP Email Test</h1>",
            "<p>This email was sent with Amazon SES using the ",
            "<a href='https://github.com/javaee/javamail'>Javamail Package</a>",
            " for <a href='https://www.java.com'>Java</a>."
    );*/

    public void sendMail(String toEmail, String subject, String message) throws Exception {

        // Create a Properties object to contain connection configuration information.
        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.port", PORT);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");

        // Create a Session object to represent a mail session with the specified properties.
        Session session = Session.getDefaultInstance(props);

        // Create a message with the specified information.
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(FROM,FROMNAME));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
        msg.setSubject(subject);
        msg.setContent(message,"text/html");

        /*msg.setHeader("X-SES-CONFIGURATION-SET", CONFIGSET);*/

        Transport transport = session.getTransport();

        // Send the message.
        try{
            System.out.println("Sending...");

            // Connect to Amazon SES using the SMTP username and password you specified above.
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);

            // Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            System.out.println("Email sent!");
        }catch (Exception e) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + e.getMessage());
        }finally{
            // Close and terminate the connection.
            transport.close();
        }
    }
}
