package com.vinbytes.binary.services;

import com.vinbytes.binary.controller.PlayBidController;
import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.OverallProgressModal;
import com.vinbytes.binary.model.PlayBidModel;
import com.vinbytes.binary.model.UserModel;
import com.vinbytes.binary.repository.OverallProgressRepository;
import com.vinbytes.binary.repository.PlayBidRepository;
import com.vinbytes.binary.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Service
public class PlayBidServices {
    @Autowired
    public PlayBidRepository playBidRepository;


    @Autowired
    private NotificationService notificationService;


    @Autowired
    public OverallProgressRepository overallProgressRepository;

    @Autowired
    private UserRepository userRepository;

    private static final Logger log = LoggerFactory.getLogger(PlayBidServices.class);

    public boolean create(PlayBidModel playBidModel) {

        if ( playBidModel.getStockName() == null
                || playBidModel.getBiddingStartsAt() == null
                || playBidModel.getBiddingClosesAt() == null
                || playBidModel.getDrawTime() == null

        ) {
            return false;
        }
        try {
            List<PlayBidModel> model= playBidRepository.findAllByBidType(playBidModel.getBidType());
            long bidId = 1001L;
            if(model.size()>0){
                String bidIdFromDB = model.get(model.size()-1).getBidId();
                String[] bidIdFromDBSplit = bidIdFromDB.split("M");
                String onlyLongBidIdFromDB = bidIdFromDBSplit[1];
                bidId = Long.parseLong(onlyLongBidIdFromDB)+1;
            }
            playBidModel.setBidId(playBidModel.getBidType()+"M"+bidId);
            /*Jul 09*/
            List<OverallProgressModal> overallProgressModalAll = overallProgressRepository.findAll();
            OverallProgressModal overallProgressModal;
            if (overallProgressModalAll.size()==0){
                overallProgressModal = new OverallProgressModal();
                overallProgressModal.setTotalNoOfBids(1.00);
            }else {
                overallProgressModal = overallProgressModalAll.get(0);
                overallProgressModal.setTotalNoOfBids(overallProgressModal.getTotalNoOfBids()+1);
            }
            if(new Date().compareTo(playBidModel.getBiddingStartsAt())<0){
                playBidModel.setBiddingStatus("Opening Soon");
                overallProgressModal.setTotalNoOfOpeningSoonBids(overallProgressModal.getTotalNoOfOpeningSoonBids()+1);
            }else if(new Date().compareTo(playBidModel.getBiddingStartsAt())>=0){
                playBidModel.setBiddingStatus("Open Now");
                overallProgressModal.setTotalNoOfInProgressBids(overallProgressModal.getTotalNoOfInProgressBids()+1);
            }else if (new Date().compareTo(playBidModel.getBiddingClosesAt())>=0){
                playBidModel.setBiddingStatus("Closed");
                overallProgressModal.setTotalNoOfClosedBids(overallProgressModal.getTotalNoOfClosedBids()+1);
            }else if (new Date().compareTo(playBidModel.getDrawTime())>=0){
                playBidModel.setBiddingStatus("Completed");
                overallProgressModal.setTotalNoOfCompletedBids(overallProgressModal.getTotalNoOfCompletedBids()+1);
            }
            playBidModel.setTotalNoOfBiddersLoss(0.00);
            playBidModel.setTotalWinningAmount(0.00);
            playBidModel.setTotalNoOfBiddersWon(0.00);
            playBidModel.setTotalAmountBid(0.00);
            playBidModel.setTotalTransactionFees(0.00);
            playBidModel.setTotalRevenue(0.00);
            playBidModel.setCreatedOn(new Date());
            playBidRepository.save(playBidModel);
            overallProgressRepository.save(overallProgressModal);
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }

    }
}
