package com.vinbytes.binary.services;

import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.*;
import com.vinbytes.binary.repository.*;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

import static java.lang.Double.parseDouble;

@Service
public class PurchaseBidsServices extends TransactionServices {
    @Autowired
    public PurchaseBidsRepository purchaseBidsRepository;

    @Autowired
    public PlayBidRepository playBidRepository;

    @Autowired
    public UserRepository userRepository;

    @Autowired
    public WalletRepository walletRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private PermissionsRepository permissionsRepository;

    @Autowired
    private CoinBaseTransactionRepository coinBaseTransactionRepository;

    @Autowired
    public OverallProgressRepository overallProgressRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private DynamicSlabRepository dynamicSlabRepository;

    public ResponseEntity<?> create(PurchaseBidsModel purchaseBidsModel) {


        if (purchaseBidsModel.getStockName() == null ||
                purchaseBidsModel.getStockValue() == null ||
                purchaseBidsModel.getChangeInStockValue() == null ||
                purchaseBidsModel.getLevel() == null ||
                purchaseBidsModel.getValueOfOneBid() == null ||
                purchaseBidsModel.getNoOfBids() == null ||
                purchaseBidsModel.getTotalAmount() == 0) {
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("Some values are missing"));
        }
        try {
            /*Jul 09*/

            String Url;
            DateFormat formatter = new SimpleDateFormat("ddMMMyyhhmmssMs");
            UserModel user = userRepository.findById(purchaseBidsModel.getUserId()).orElse(null);
            if (user == null) {
                return ResponseEntity.badRequest()
                        .body(new MessageResponse("User Not Available !"));
            } else {
                PlayBidModel play = playBidRepository.findByBidId(purchaseBidsModel.getStockId());
                if (play == null) {
                    return ResponseEntity.badRequest()
                            .body(new MessageResponse("Bid Not Available !"));
                } else if (new Date().compareTo(play.getBiddingClosesAt()) >= 0) {
                    return ResponseEntity.badRequest()
                            .body(new MessageResponse("Bid Closed !"));
                } else {
                    WalletModel userWallet = walletRepository.findByUserId(purchaseBidsModel.getUserId());
//                   Double purchaseAmount = purchaseBidsModel.getTotalAmount();

                    Double CurrentBTCPrice = getCurrentCoinbaseValue("https://www.coinbase.com/api/v2/assets/prices/bitcoin?base=USD");
                    double valueOfOneBid = parseDouble(play.getValueOfOneBid())/CurrentBTCPrice;
                    Double purchaseAmount = parseDouble(purchaseBidsModel.getNoOfBids()) * valueOfOneBid;
                    Double BiddingAmount = parseDouble(purchaseBidsModel.getNoOfBids()) * valueOfOneBid;
                    double CurrentBTCPriceForPreAmount = 5.00/CurrentBTCPrice;
                    String exchangeRateString = "1BTC@USD"+CurrentBTCPrice;
//                    play.setValueOfOneBid(String.valueOf(parseDouble(play.getValueOfOneBid())/CurrentBTCPrice));
                /*    Double purchaseAmount = (parseDouble(purchaseBidsModel.getNoOfBids()) * parseDouble(play.getValueOfOneBid()));
                    Double BiddingAmount = ((parseDouble(purchaseBidsModel.getNoOfBids())) * (parseDouble(purchaseBidsModel.getValueOfOneBid())));*/
                    /*Double BiddingTransactionFee = ((parseDouble(purchaseBidsModel.getNoOfBids())) * 0.000011);*/
                    /*play.setTotalAmountBid(play.getTotalAmountBid()+BiddingAmount+(18*CurrentBTCPriceForPreAmount));*/
                    play.setTotalAmountBid(play.getTotalAmountBid()+BiddingAmount);
                    if (play.getTotalNoOfBidsPurchased() == null) {
                        play.setTotalNoOfBidsPurchased(parseDouble(purchaseBidsModel.getNoOfBids()));
                    } else {
                        play.setTotalNoOfBidsPurchased(play.getTotalNoOfBidsPurchased() + parseDouble(purchaseBidsModel.getNoOfBids()));
                    }

                    /*play.setTotalTransactionFees(play.getTotalTransactionFees() + BiddingTransactionFee);*/
                    play.setTotalRevenue(play.getTotalRevenue() + purchaseAmount);
                    if (!(userWallet.getDollars() >= purchaseAmount)) {
                        return ResponseEntity.badRequest()
                                .body(new MessageResponse("Insufficient Balance. please add money in wallet"));
                    } else {
                        List<PurchaseBidsModel> playFromDb = purchaseBidsRepository.findAllByStockIdAndUserId(purchaseBidsModel.getStockId(), purchaseBidsModel.getUserId());

                        if (playFromDb.size() >= 3) {
                            return ResponseEntity.badRequest()
                                    .body(new MessageResponse("Reached maximum purchase limit for this bid "));
                        } else {
                            /*Jul 09*/
                            int noOfBids = Integer.parseInt(purchaseBidsModel.getNoOfBids());
                          /*  List<Integer> unitsList = Arrays.asList(5,10,25,50,100,500,1000,5000);
                            if(!unitsList.contains(noOfBids))
                            {
                                return ResponseEntity.badRequest()
                                        .body(new MessageResponse("Invalid request"));
                            }*/

                            TransactionModal transactionModal = new TransactionModal();
                            transactionModal.setTotalAmount(purchaseAmount);
                            transactionModal.setCreditedUnitBtc(purchaseAmount);
                            transactionModal.setUserId(user.getId());
                            transactionModal.setStockId(purchaseBidsModel.getStockId());
                            transactionModal.setStockName(purchaseBidsModel.getStockName());
                            transactionModal.setStockSymbol(purchaseBidsModel.getSymbol());
                            transactionModal.setUnitsPurchased(purchaseBidsModel.getNoOfBids());
                            transactionModal.setUserName(user.getUsername());
                            transactionModal.setTransactionMode("Wallet");
                            transactionModal.setTransactionType("Bid Purchased");
                            transactionModal.setTransactionStatus("Purchased");
                            transactionModal.setExchangeRateString(exchangeRateString);
//                             transactionModal.setTransactionStatus("Pending");
                            Date transactionInitiateDateAndTime = new Date();
                            transactionModal.setTransactionInitiateDateAndTime(transactionInitiateDateAndTime);
                            String whatWasThis = transactionModal.getTransactionType();
                            String transactionId = formatter.format(transactionInitiateDateAndTime) + (new Random().nextInt(900) + 100);
                            if (whatWasThis.equals("Bid Purchased")) {
                                transactionId = "PRD" + transactionId;
                            }
                           /* else{
                                if (whatWasThis.equals("Add Money")){
                                    transactionId = "ADD" + transactionId;
                                }else {
                                    transactionId = "WDL" + transactionId;
                                }
                            }*/
                            transactionModal.setTransactionId(transactionId);
//                            boolean transactionStatus = MadeTransaction(transactionModal);
                            transactionModal.setExchangeRate1BTC(CurrentBTCPrice);
                            transactionRepository.save(transactionModal);


                            OverallProgressModal overallProgressModal = overallProgressRepository.findAll().get(0);
                            userWallet.setDollars(userWallet.getDollars() - purchaseAmount);
                            /*userWallet.setTotalTransactionFee(userWallet.getTotalTransactionFee() + BiddingAmount);*/
                            userWallet.setTotalBitAmount(userWallet.getTotalBitAmount());
                            userWallet.setTotalNumberOfBitsPurchased(userWallet.getTotalNumberOfBitsPurchased() + 1);
                            userWallet.setTotalDebit(userWallet.getTotalDebit() + purchaseAmount);
                            userWallet.setTotalNumberOfTimesDebit(userWallet.getTotalNumberOfTimesDebit() + 1);
                            walletRepository.save(userWallet);
                            overallProgressModal.setTotalRevenueGenerated(overallProgressModal.getTotalRevenueGenerated() + purchaseAmount);
                            overallProgressModal.setTotalAmountCollectedForBids(overallProgressModal.getTotalAmountCollectedForBids() + BiddingAmount);
                            /*overallProgressModal.setTotalAmountOfTransactionFeeForBidsPurchased(overallProgressModal.getTotalAmountOfTransactionFeeForBidsPurchased());*/
                            overallProgressRepository.save(overallProgressModal);
                            purchaseBidsModel.setBiddingDate(LocalDateTime.now(ZoneId.of("Asia/Kolkata")));
                            purchaseBidsModel.setPurchaseStatus("Success");
                            purchaseBidsModel.setStatus("waiting for Results");
                            purchaseBidsModel.setTransactionId(transactionId);
                            purchaseBidsModel.setExchangeRateString(exchangeRateString);
                            purchaseBidsModel.setUserId(purchaseBidsModel.getUserId());
                            purchaseBidsModel.setBiddingAmount(String.valueOf(purchaseAmount));
                            purchaseBidsModel.setStockValue(String.valueOf(valueOfOneBid));
                            purchaseBidsModel.setValueOfOneBid(String.valueOf(valueOfOneBid));
                            purchaseBidsModel.setTotalAmount(purchaseAmount);
                            purchaseBidsModel.setExchangeRate1BTC(CurrentBTCPrice);
                            purchaseBidsRepository.save(purchaseBidsModel);

                 /*               try {
                                    URL url = new URL("http://localhost:3000/deposit");
                                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                                    con.setRequestMethod("POST");
                                    con.setRequestProperty("Content-Type", "application/json");
                                    con.setDoOutput(true);
                                    JSONParser parser = new JSONParser();
//                                Double purchaseAmount = (((parseDouble(purchaseBidsModel.getNoOfBids()))*(parseDouble(purchaseBidsModel.getValueOfOneBid())))+((parseDouble(purchaseBidsModel.getNoOfBids()))*0.5));
                                    JSONObject jsonInput = (JSONObject) parser.parse("{'amount':" + purchaseAmount + "}");
                                    con.getOutputStream().write(jsonInput.toJSONString().getBytes(StandardCharsets.UTF_8));
                                    Reader streamReader = null;
                                    boolean Error = false;
                                    int status = con.getResponseCode();
                                    if (status > 299) {
                                        streamReader = new InputStreamReader(con.getErrorStream());
                                        Error = true;
                                    } else {
                                        streamReader = new InputStreamReader(con.getInputStream());
                                    }
                                    BufferedReader in = new BufferedReader(streamReader);
                                    String inputLine;
                                    StringBuilder content = new StringBuilder();
                                    while ((inputLine = in.readLine()) != null) {
                                        content.append(inputLine);
                                    }
                                    in.close();
                                    con.disconnect();
                                    if (Error) {
                                        return ResponseEntity.badRequest()
                                                .body(new MessageResponse("Url Not Generate Successfully1"));

                                    } else {
                                        UserModel userFromDb = userRepository.findById(user.getId()).orElse(null);
                                        if (userFromDb == null) {
                                            return ResponseEntity.badRequest()
                                                    .body(new MessageResponse("Url Not Generate Successfully, User Not Found !"));

                                        } else {

                                            JSONObject json = (JSONObject) parser.parse(String.valueOf(content));
                                            Url = (String) json.get("hosted_url");
                                            String code = (String) json.get("code");
                                            DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                            Date Expires = formatter1.parse((String) json.get("expires_at"));
                                            Date Created = formatter1.parse((String) json.get("created_at"));
                                            CoinBaseTransactionModal coinBaseTransactionModal = new CoinBaseTransactionModal();
//                                            coinBaseTransactionModal.setTransactionId(code);
                                            coinBaseTransactionModal.setTransactionId(transactionId);
                                            coinBaseTransactionModal.setCoinBaseTransactionId(code);
                                            coinBaseTransactionModal.setCoinBaseTransactionHostedUrl(Url);
                                            coinBaseTransactionModal.setUserId(user.getId());
                                            coinBaseTransactionModal.setUserName(user.getUsername());
                                            coinBaseTransactionModal.setStockId(purchaseBidsModel.getStockId().toString());
                                            coinBaseTransactionModal.setStockName(purchaseBidsModel.getStockName());
                                            coinBaseTransactionModal.setStockSymbol(purchaseBidsModel.getSymbol());
                                            coinBaseTransactionModal.setTransactionMode("CoinBase");
                                            coinBaseTransactionModal.setUnitsPurchased(purchaseBidsModel.getNoOfBids());
                                            coinBaseTransactionModal.setCoinBaseTransactionHostedUrlExpires(Expires);
                                            coinBaseTransactionModal.setTransactionStatus("Pending");
                                            coinBaseTransactionModal.setPaymentCreatedAt(Created);
                                            coinBaseTransactionModal.setChangesBtw(purchaseBidsModel.getChangesBtw());
                                            coinBaseTransactionModal.setLevel(purchaseBidsModel.getLevel());
                                            coinBaseTransactionModal.setValueOfOneBid(play.getValueOfOneBid());
                                            coinBaseTransactionModal.setTransactionFee(BiddingTransactionFee.toString());
                                            coinBaseTransactionModal.setBiddingAmount(BiddingAmount.toString());
                                            coinBaseTransactionModal.setTotalAmount(purchaseAmount);
                                            coinBaseTransactionModal.setConfirmReceivedPayment(encoder.encode("false"));
                                            purchaseBidsModel.setCoinBasePurchaseId((String) json.get("code"));
                                            purchaseBidsModel.setCoinBaseUrl(Url);
                                            purchaseBidsRepository.save(purchaseBidsModel);
                                            PurchaseBidsModel frmDb=purchaseBidsRepository.findByCoinBasePurchaseId((String) json.get("code"));
                                            coinBaseTransactionModal.setUserPurchaseId(frmDb.getId());

                                            coinBaseTransactionRepository.save(coinBaseTransactionModal);

                                        }
                                    }
                                }
                                catch (IOException | ParseException | java.text.ParseException e){
                                    System.out.println(e);
                                    return ResponseEntity.badRequest()
                                            .body(new MessageResponse("Url Not Generate Successfully2"));


                                }*/


                            List<String> userid = new ArrayList<>();
                                if(play.getUserId()!=null)
                                {
                                    if (play.getUserId().size()>0) {
                                        userid = play.getUserId();
                                    }
                                    if(userid.size()>0)
                                    {
                                        if (!userid.contains(purchaseBidsModel.getUserId())) {
                                            userid.add(purchaseBidsModel.getUserId());

                                        }
                                    }else {
                                        userid.add(purchaseBidsModel.getUserId());
                                    }
                                }else {
                                    userid.add(purchaseBidsModel.getUserId());
                                }

                                play.setUserId(userid);

                            PurchaseBidsModel purchaseFrmDb = purchaseBidsRepository.findByTransactionId(transactionModal.getTransactionId());
                            List<Double> ArrayOfIntForFindingMaxValue = new ArrayList<Double>(Collections.nCopies(8, 0.00));
                            if (play.getTotalNoOfAmountInSlab() != null && play.getTotalNoOfAmountInSlab().size() != 0) {
                                ArrayOfIntForFindingMaxValue = play.getTotalNoOfAmountInSlab();
                            }
                            String level = purchaseFrmDb.getLevel();
                               int whichoneoftheslab = 0;
                                DynamicSlabModel dynamicSlabModel = dynamicSlabRepository.findByType(play.getBidType());
                                List<DynamicSlabValues> dynamicSlabValuesList = dynamicSlabModel.getDynamicSlabValues();
                                List<String> slabs = new ArrayList<>();
                                for (DynamicSlabValues dynamicSlabValues : dynamicSlabValuesList){
                                    long fromValue = Math.round(dynamicSlabValues.getFromValue());
                                    long toValue = Math.round(dynamicSlabValues.getToValue());
                                    String whichSlab = null;
                                    if (fromValue == toValue){
                                        whichSlab = "over $" + fromValue;
                                    }else {
                                        whichSlab = "$" + fromValue + " - $" + toValue;
                                    }
                                    slabs.add(whichSlab);
                                }

                                if (purchaseFrmDb.getChangesBtw().equals(slabs.get(0))){
                                    if (level.equals("Up")){
                                        whichoneoftheslab = 0;
                                    }else {
                                        whichoneoftheslab = 1;
                                    }

                                }else if (purchaseFrmDb.getChangesBtw().equals(slabs.get(1))){
                                    if (level.equals("Up")){
                                        whichoneoftheslab = 2;
                                    }else {
                                        whichoneoftheslab = 3;
                                    }

                                }else if (purchaseFrmDb.getChangesBtw().equals(slabs.get(2))){
                                    if (level.equals("Up")){
                                        whichoneoftheslab = 4;
                                    }else {
                                        whichoneoftheslab = 5;
                                    }

                                }else if (purchaseFrmDb.getChangesBtw().equals(slabs.get(3))){
                                    if (level.equals("Up")){
                                        whichoneoftheslab = 6;
                                    }else {
                                        whichoneoftheslab = 7;
                                    }

                                }else if (purchaseFrmDb.getChangesBtw().equals(slabs.get(4))){
                                    if (level.equals("Up")){
                                        whichoneoftheslab = 8;
                                    }else {
                                        whichoneoftheslab = 9;
                                    }

                                }else if (purchaseFrmDb.getChangesBtw().equals(slabs.get(5))){
                                    if (level.equals("Up")){
                                        whichoneoftheslab = 10;
                                    }else {
                                        whichoneoftheslab = 11;
                                    }

                                }else if (purchaseFrmDb.getChangesBtw().equals(slabs.get(6))){
                                    if (level.equals("Up")){
                                        whichoneoftheslab = 12;
                                    }else {
                                        whichoneoftheslab = 13;
                                    }

                                }else if (purchaseFrmDb.getChangesBtw().equals(slabs.get(7))){
                                    if (level.equals("Up")){
                                        whichoneoftheslab = 14;
                                    }else {
                                        whichoneoftheslab = 15;
                                    }

                                }else if (purchaseFrmDb.getChangesBtw().equals(slabs.get(8))){
                                    if (level.equals("Up")){
                                        whichoneoftheslab = 16;
                                    }else {
                                        whichoneoftheslab = 17;
                                    }

                                }else {
                                    System.out.println("error, changes between -  default statement !");
                                }

                            double ValueToBeAdded = 0.00;
                            if (ArrayOfIntForFindingMaxValue.size() > 0) {

                                if (ArrayOfIntForFindingMaxValue.get(whichoneoftheslab) != null) {
                                    ValueToBeAdded = ArrayOfIntForFindingMaxValue.get(whichoneoftheslab) + Double.parseDouble(purchaseFrmDb.getBiddingAmount());

                                } else {
                                    ValueToBeAdded = Double.parseDouble(purchaseFrmDb.getBiddingAmount());
                                }

                                ArrayOfIntForFindingMaxValue.set(whichoneoftheslab, ValueToBeAdded);

                            }

                            else {
                                    System.out.println("error, missing");
                                }
                                play.setTotalNoOfAmountInSlab(ArrayOfIntForFindingMaxValue);

                                playBidRepository.save(play);

                          /*  List<PurchaseBidsModel> playFromDb2 = purchaseBidsRepository.findAllByStockIdAndUserId(purchaseBidsModel.getStockId(), purchaseBidsModel.getUserId());
                            List<String> playId = new ArrayList<>();
                            if (user.getPurchaseId() != null) {
                                if (user.getPurchaseId().size() > 0) {
                                    playId = user.getPurchaseId();
                                }
                                if (playId.size() > 0) {
                                    if (!playId.contains(playFromDb2.get(playFromDb2.size() - 1).getId())) {
                                        playId.add(playFromDb2.get(playFromDb2.size() - 1).getId());
                                    }
                                } else {
                                    playId.add(playFromDb2.get(playFromDb2.size() - 1).getId());
                                }
                            } else {
                                playId.add(playFromDb2.get(playFromDb2.size() - 1).getId());
                            }
                            user.setPurchaseId(playId);

                            userRepository.save(user);
*/                         notificationService.MakeNotifications(user.getId(),purchaseBidsModel.getStockId()+" purchased successfully.", "", play.getImageUrl(), play.getBidId(), purchaseBidsModel.getTransactionId());

                           return ResponseEntity.ok().body(purchaseFrmDb);



                        }

                    }
                }
            }

        } catch (Exception e) {

            return ResponseEntity.badRequest()
                    .body(new MessageResponse("Bad Request"));
        }
    }


    Double getCurrentCoinbaseValue(String urlString){
        try {
            URL url = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            Reader streamReader = null;
            boolean Error = false;
            int status = con.getResponseCode();
            if (status > 299) {
                streamReader = new InputStreamReader(con.getErrorStream());
                Error = true;
            } else {
                streamReader = new InputStreamReader(con.getInputStream());
            }
            BufferedReader in = new BufferedReader(streamReader);
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();
            double CurrentPrice = 0.00;
            if (!Error){
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(String.valueOf(content));
                JSONObject data = (JSONObject) json.get("data");
                JSONObject prices = (JSONObject) data.get("prices");
                CurrentPrice = Double.parseDouble((String) prices.get("latest"));
            }
            return CurrentPrice;
        }catch (IOException | ParseException e){
            return 0.00;
        }
    }
}
