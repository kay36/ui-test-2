package com.vinbytes.binary.services;

import com.vinbytes.binary.model.TransactionModal;
import com.vinbytes.binary.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Service
public class TransactionServices {

    @Autowired
    private TransactionRepository transactionRepository;

    private static final Logger log = LoggerFactory.getLogger(TransactionServices.class);

    public Boolean MadeTransaction(TransactionModal transactionModal){
        log.info("inside TransactionService.MadeTransaction() method");
        try{
            String whatWasThis = transactionModal.getTransactionType();
            Date transactionInitiateDateAndTime = new Date();
            DateFormat formatter = new SimpleDateFormat("ddMMMyyhhmmssMs");
            transactionModal.setTransactionInitiateDateAndTime(transactionInitiateDateAndTime);
            String transactionId = formatter.format(transactionInitiateDateAndTime) + (new Random().nextInt(900)+100);
            if (whatWasThis.equals("Bid Purchased")){
                transactionId = transactionModal.getStockSymbol() + transactionId;

            }else{
                transactionId = "CRD" + transactionId;
            }
            transactionModal.setTransactionId(transactionId);
            transactionRepository.save(transactionModal);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
