
package com.vinbytes.binary.message.response;

import org.keycloak.representations.AccessTokenResponse;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class JwtResponse implements Serializable {

	private static final long serialVersionUID = -5831397033480755831L;

	private AccessTokenResponse token;
	private String type = "Bearer";
	private String id;
	private String username;
	private String countryCode;
	private String email;
	private String roles;
	private String phoneNo;
	private String profileUrl;
	private String otp;
	private boolean verified;
	private boolean resetPassword;
	private String loginType;
	private boolean deleted;
	private String googleUserId;
	private String cryptoAddress;


	public JwtResponse(AccessTokenResponse token,String id, String username,String email, String roles, String countryCode,
					   String phoneNo, String profileUrl, String otp, boolean verified, boolean resetPassword, String loginType,boolean deleted,String cryptoAddress) {
		this.token = token;
		this.id=id;
		this.username = username;
		this.email = email;
		this.roles = roles;
		this.countryCode = countryCode;
		this.phoneNo = phoneNo;
		this.profileUrl = profileUrl;
		this.otp = otp;
		this.verified = verified;
		this.resetPassword = resetPassword;
		this.loginType = loginType;
		this.deleted=deleted;
		this.cryptoAddress=cryptoAddress;
	}

	public JwtResponse(AccessTokenResponse jwt, String roles, String email,String id, String username, String googleUserId) {
		this.token = jwt;
		this.email = email;
		this.id = id;
		this.roles = roles;
		this.username = username;
		this.googleUserId = googleUserId;
	}
	public JwtResponse(AccessTokenResponse jwt, String roles, String email,String id, String username, String googleUserId, String profileUrl) {
		this.token = jwt;
		this.email = email;
		this.id = id;
		this.roles = roles;
		this.username = username;
		this.googleUserId = googleUserId;
		this.profileUrl=profileUrl;
	}



	//    public JwtResponse(String jwt, String id, String username, String email, Collection<? extends GrantedAuthority> roles, String phoneNo, String profileUrl, String otp, boolean verified, boolean resetPassword, String userType, boolean deleted) {
//    }


	public String getCryptoAddress() {
		return cryptoAddress;
	}

	public void setCryptoAddress(String cryptoAddress) {
		this.cryptoAddress = cryptoAddress;
	}

	public AccessTokenResponse getToken() {
		return token;
	}

	public void setToken(AccessTokenResponse token) {
		this.token = token;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public boolean isResetPassword() {
		return resetPassword;
	}

	public void setResetPassword(boolean resetPassword) {
		this.resetPassword = resetPassword;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getGoogleUserId() {
		return googleUserId;
	}

	public void setGoogleUserId(String googleUserId) {
		this.googleUserId = googleUserId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}
